<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-3">
		<h2 class="text-center">Gestión de calificaciones</h2>
		<h4 class="text-center">Estudiantes</h4>

		<table class="table table-hover table-stripped text-center">
			<th>Cédula</th>
			<th>Nombres</th>
			<th>Apellidos</th>
			<?php foreach($estudiantes as $estudiante){ ?>
				<tr class="link">
					<td>
						<a class="d-block link" href="<?php echo SERVERURL ?>/calificaciones/estudiante/<?php echo $id_grado . '/' . $numero . '/' . $estudiante->cedula ?>/"><?php echo $estudiante->cedula ?></a>
					</td>
					<td>
						<a class="d-block link" href="<?php echo SERVERURL ?>/calificaciones/estudiante/<?php echo $id_grado . '/' . $numero . '/' . $estudiante->cedula ?>/"><?php echo $estudiante->nombres ?></a>
					</td>
					<td>
						<a class="d-block link" href="<?php echo SERVERURL ?>/calificaciones/estudiante/<?php echo $id_grado . '/' . $numero . '/' . $estudiante->cedula ?>/"><?php echo $estudiante->apellidos ?></a>
					</td>
				</tr>
			<?php } ?>
			<?php if(count($estudiantes) < 1){ ?>
				<tr>
					<td colspan="3">No hay estudiantes registrados.</td>
				</tr>
			<?php } ?>
		</table>
		
		<a class="d-block mx-auto w-25" href="<?php echo SERVERURL ?>/menu/">
			<button class="btn btn-info d-block mx-auto mt-5 w-100" type="button">Menú principal</button>
		</a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>