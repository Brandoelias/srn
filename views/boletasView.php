<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<div class="no-print">
		<?php include_once('./views/partials/header.php') ?>
	</div>

	<main class="p-3 text-center menu-items no-print">

		<div class="jumbotron p-5 d-block mx-auto w-25 text-center menu-item2 boletaPorSeccion">
			Por sección
		</div>
		<div class="jumbotron p-5 d-block mx-auto w-25 text-center menu-item2 boletaPorEstudiante">
			Por estudiante
		</div>

	</main>


	<div id="boletaGenerada" class="d-none print"></div>

	<div class="no-print">
		<?php include_once('./views/partials/footer.php') ?>
	</div>

</body>
</html>