<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main>
		<div class="jumbotron p-5 d-block mx-auto w-25 text-center loginBox">
			<span class="text-primary loginText">Iniciar Sesión</span>
			<div id="loginErrorAlert" class="alert alert-danger text-center mx-auto w-100 mb-4 mt-2 rebote d-none">¡Datos incorrectos!</div>
			<form method="POST" id="loginForm">
				<div class="form-group">
					<input id="usuario" class="form-control mt-3" type="text" placeholder="Usuario..." required />
				</div>
				<div class="form-group">
					<input id="pass" class="form-control mt-2" type="password" placeholder="Contraseña..." required />
				</div>
				<input id="loginButton" class="btn btn-primary mt-2" type="submit" value="Ingresar" />
			</form>
		</div>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>