const SERVERURL = 'https://registrodenotas.herokuapp.com';
//const SERVERURL = 'http://zitro/SRN';

const alerta = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-success ml-2 mr-2',
    cancelButton: 'btn btn-danger ml-2 mr-2',
  },
  showClass: {
  	popup: 'swal-animation-show',
	backdrop: 'swal-animation-show',
	icon: 'swal-animation-show'
  },
  buttonsStyling: false,
  cancelButtonText: 'Cancelar',
  allowOutsideClick: () => !Swal.isLoading()
})

//funciones
function enviandoFormulario(boton){
	var guardarBotonClass = $(boton).attr('class');
	$('#myModal').css('pointer-events', 'none');
	$('#cancelar, input').attr('disabled', 'disabled');
	$(boton).removeClass(guardarBotonClass);
	$(boton).text('');
	$(boton).blur();
	$(boton).attr('disabled', 'disabled');
	$(boton).css('margin-left', '50px');
	$(boton).addClass('spinner-border text-success');
}

function formularioEnviado(res, boton, msj, msjError){
	$('#formularioExito, #formularioError').addClass('d-none');
	if(res == 'ok'){
		$('.form-modal').addClass('d-none');
		$(boton).addClass('d-none');
		$('#cancelar').addClass('d-none');
		$('form').val('');
		$('#cerrar').removeClass('d-none');
		$('#formularioExito').text(msj);
		$('#formularioExito').removeClass('d-none');
	}else{
		$('#formularioError').text(msjError);
		$('#formularioError').removeClass('d-none');
		$(boton).removeAttr('disabled');
		$(boton).css('margin-left', '');
		$(boton).removeClass('spinner-border text-success');
		$(boton).addClass('btn btn-success');
		$(boton).text('Guardar');
		$('#myModal').css('pointer-events', '');
		$('#cancelar, input').removeAttr('disabled');
	}
	
}

function calendario(startDate = '01/01/1995', minYear = '1901', maxYear = null, minDate = null){

	if(maxYear == null){
		maxYear = parseInt(moment().format('YYYY'),10);
	}
	if(minDate == null){
		minDate = '01/01/1901';
	}

	$(function() {
  		$('input[name="fecha"]').daterangepicker({
    		singleDatePicker: true,
	 		showDropdowns: true,
			startDate: startDate,
    		minYear: minYear,
    		maxYear: maxYear,
    		minDate: minDate,
    		autoUpdateInput: false,
    		parentEl: '.swal-overlay',
    		locale: {
		        separator: " - ",
		        applyLabel: "Ok",
		        cancelLabel: "Cancelar",
		        fromLabel: "Desde",
		        toLabel: "Hasta",
		        customRangeLabel: "Elegir",
		        daysOfWeek: [
		            "Dom",
		            "Lun",
		            "Mar",
		            "Mié",
		            "Jue",
		            "Vie",
		            "Sáb"
		        ],
		        monthNames: [
		            "Enero",
		            "Febrero",
		            "Marzo",
		            "Abril",
		            "Mayo",
		            "Junio",
		            "Julio",
		            "Agosto",
		            "Septiembre",
		            "Octubre",
		            "Noviembre",
		            "Deciembre"
		        ],
		        firstDay: 1,
		        format: 'DD/MM/Y'
		    }
  		});
	});


	$('input[name="fecha"]').on('apply.daterangepicker', function(ev, picker) {
    	$(this).val(picker.startDate.format('DD/MM/YYYY'));
    	$(this).parents('.form-group').addClass('focused');
  	});
	

}


//Event handlers
$(document).ready(function(){


	//Inicio de sesion
	$('#loginForm').on('submit', function(e){
		e.preventDefault();
		$('#loginErrorAlert').addClass('d-none');
		$('#loginButton').addClass('disabled');
		var usuario = $('#usuario').val();
		var pass = $('#pass').val();
		$.post(SERVERURL + '/login/check', {usuario : usuario, pass : pass}, function(res){
			if(res == 'ok'){
				window.location.href = SERVERURL + '/menu/';
			}else{
				$('#loginErrorAlert').removeClass('d-none');
				$('#loginButton').removeClass('disabled');
			}
		});
	});


	//Redirigir al correspondiente item del menú
	$('.menu-item').click(function(){
		var item = $(this).attr('link').trim().toLowerCase();
		window.location.href = `${SERVERURL}/${item}/`;
	})

	//Agregar Profesor
	$('.agregarProfesor').click(function(){
		alerta.fire({
		title: 'Agregar profesor',
		html: `
			<div id="alertaError" class="alert alert-danger alert-dismissible text-center d-none">
			  <strong>¡Error!</strong> <span>Faltan datos por llenar.</span>
			</div>
			<div class="form-inline mx-auto text-center d-block w-100">
				<div class="form-group">
					<div class="input-title">Cédula</div>
					<input id="cedula" class="form-control w-75" type="number"/>
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Nombres</div>
					<input id="nombres" class="form-control w-75" />
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Apellidos</div>
					<input id="apellidos" class="form-control w-75" />
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Teléfono</div>
					<input id="telefono" class="form-control w-75" />
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Correo</div>
					<input id="correo" class="form-control w-75" type="email" />
				</div>
			</div>
		`,
		showCancelButton: true,
		confirmButtonText: 'Guardar',
		showLoaderOnConfirm: true,
		preConfirm: ()=>{
			return new Promise(function(resolve){

				var cedula = $('#cedula').val().trim();
				var nombres = $('#nombres').val().trim();
				var apellidos = $('#apellidos').val().trim();
				var telefono = $('#telefono').val().trim();
				var correo = $('#correo').val().trim();
				var patron = /^[a-zA-ZÀ-ú\s]+$/;
				var soloLetras = new RegExp(patron);

				if(cedula == '' || nombres == '' || apellidos == '' || telefono == '' || correo == ''){
					$('#alertaError span').text('Faltan datos por llenar.');
					$('#alertaError').removeClass('d-none');
					$('input').each((i)=>{
						if($('input').get(i).value == ''){
							$($('input').get(i)).css('border', '1px solid red');
							$($('input').get(i)).on('change', function(){
								if($('input').get(i).value != ''){
									$($('input').get(i)).css('border', '');
								}else{
									$($('input').get(i)).css('border', '1px solid red');
									todosLlenos = false;
								}
							});
						}
					})
					Swal.disableLoading();
					return;
				}
				if(!soloLetras.test(nombres)){
					$('#alertaError span').text('El nombre contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#nombres').css('border', '1px solid red');
					$('#nombres').on('change', function(){
						if($(this).val() != '' && soloLetras.test($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}
				if(!soloLetras.test(apellidos)){
					$('#alertaError span').text('El apellido contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#apellidos').css('border', '1px solid red');
					$('#apellidos').on('change', function(){
						if($(this).val() != '' && soloLetras.test($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}
				if(!soloLetras.test(nombres)){
					$('#alertaError span').text('El nombre contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#nombres').css('border', '1px solid red');
					$('#nombres').on('change', function(){
						if($(this).val() != '' && soloLetras.test($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}
				if(!$.isNumeric(telefono)){
					$('#alertaError span').text('El teléfono contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#telefono').css('border', '1px solid red');
					$('#telefono').on('change', function(){
						if($(this).val() != '' && $.isNumeric($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}

				var datos = {cedula : cedula, nombres : nombres, apellidos : apellidos, telefono : telefono, correo : correo};

				$.post(SERVERURL + '/profesores/guardar', datos, function(res){
					if(res == 'ok'){
						alerta.fire({
							title: '¡Profesor Agregado!'
						})
						.then(()=>{
							window.location.reload();
						})
					}else{
						alerta.fire({
							title: '¡No se pudo agregar el profesor!'
						})
					}
				});

			});
		}
		})
	})



	//Editar Profesor
	$('.editarProfesor').click(function(){
		var boton = $(this);
		var cedulaOriginal = $(boton).parent().parent().find('.cedula').text().trim();
		var nombres = $(boton).parent().parent().find('.nombres').text().trim();
		var apellidos = $(boton).parent().parent().find('.apellidos').text().trim();
		var telefono = $(boton).parent().parent().find('.telefono').text().trim();
		var correo = $(boton).parent().parent().find('.correo').text().trim();
		alerta.fire({
		title: 'Editar profesor',
		html: `
			<div id="alertaError" class="alert alert-danger text-center d-none">
			  <strong>¡Error!</strong> <span>Faltan datos por llenar.</span>
			</div>
			<div class="form-inline mx-auto text-center d-block w-100">
				<div class="form-group">
					<div class="input-title">Cédula</div>
					<input id="cedula" class="form-control w-75" type="number"/>
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Nombres</div>
					<input id="nombres" class="form-control w-75" />
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Apellidos</div>
					<input id="apellidos" class="form-control w-75" />
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Teléfono</div>
					<input id="telefono" class="form-control w-75" />
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Correo</div>
					<input id="correo" class="form-control w-75" type="email" />
				</div>
			</div>
		`,
		showCancelButton: true,
		confirmButtonText: 'Guardar',
		showLoaderOnConfirm: true,
		preConfirm: ()=>{
			return new Promise(function(resolve){
				var cedulaModificar = $('#cedula').val().trim();
				var nombresM = $('#nombres').val().trim();
				var apellidosM = $('#apellidos').val().trim();
				var telefonoM = $('#telefono').val().trim();
				var correoM = $('#correo').val().trim();
				var patron = /^[a-zA-ZÀ-ú\s]+$/;
				var soloLetras = new RegExp(patron);

				if(cedulaModificar == '' || nombresM == '' || apellidosM == '' || telefonoM == '' || correoM == ''){
					$('#alertaError span').text('Faltan datos por llenar.');
					$('#alertaError').removeClass('d-none');
					$('input').each((i)=>{
						if($('input').get(i).value == ''){
							$($('input').get(i)).css('border', '1px solid red');
							$($('input').get(i)).on('change', function(){
								if($('input').get(i).value != ''){
									$($('input').get(i)).css('border', '');
								}else{
									$($('input').get(i)).css('border', '1px solid red');
									todosLlenos = false;
								}
							});
						}
					})
					Swal.disableLoading();
					return;
				}
				if(cedulaModificar == cedulaOriginal && nombres ==  nombresM && apellidos == apellidosM && telefono == telefonoM && correo == correoM){
					$('#alertaError span').text('No se cambió ningún dato.');
					$('#alertaError').removeClass('d-none');
					Swal.disableLoading();
					return;
				}
				if(!soloLetras.test(nombresM)){
					$('#alertaError span').text('El nombre contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#nombres').css('border', '1px solid red');
					$('#nombres').on('change', function(){
						if($(this).val() != '' && soloLetras.test($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}
				if(!soloLetras.test(apellidosM)){
					$('#alertaError span').text('El apellido contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#apellidos').css('border', '1px solid red');
					$('#apellidos').on('change', function(){
						if($(this).val() != '' && soloLetras.test($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}
				if(!soloLetras.test(nombresM)){
					$('#alertaError span').text('El nombre contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#nombres').css('border', '1px solid red');
					$('#nombres').on('change', function(){
						if($(this).val() != '' && soloLetras.test($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}
				if(!$.isNumeric(telefonoM)){
					$('#alertaError span').text('El teléfono contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#telefono').css('border', '1px solid red');
					$('#telefono').on('change', function(){
						if($(this).val() != '' && $.isNumeric($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}

				var datos = {cedulaOriginal : cedulaOriginal, cedulaModificar : cedulaModificar, nombres : nombresM, apellidos : apellidosM, telefono : telefonoM, correo : correoM};

				$.post(SERVERURL + '/profesores/editar', datos, function(res){
					if(res == 'ok'){
						alerta.fire({
							title: '¡Profesor Editado!'
						})
						.then(()=>{
							window.location.reload();
						})
					}else{
						alerta.fire({
							title: '¡No se pudo editar el profesor!'
						})
					}
				});

			});
		}
		})
		$('#cedula').val(cedulaOriginal);
		$('#nombres').val(nombres);
		$('#apellidos').val(apellidos);
		$('#telefono').val(telefono);
		$('#correo').val(correo);
	})


	//Borrar profesor
	$('.borrarProfesor').click(function(){
		var cedula = $(this).parent().parent().find('.cedula').text().trim();
		var nombres = $(this).parent().parent().find('.nombres').text().trim();
		var apellidos = $(this).parent().parent().find('.apellidos').text().trim();
		alerta.fire({
			title: `¿Seguro de eliminar el profesor ${nombres} ${apellidos}?`,
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					$.post(SERVERURL + '/profesores/borrar', {cedula : cedula}, function(res){
						if(res == 'ok'){
						alerta.fire({
							title: '¡Profesor Borrado!'
						})
						.then(()=>{
							window.location.reload();
						})
						}else{
							alerta.fire({
								title: '¡No se pudo borrar el profesor!'
							})
						}
					});
				});
			}
		})
	});


	//Agregar estudiante
	$('.agregarEstudiante').click(function(){
		alerta.fire({
		title: 'Agregar estudiante',
		html: `
			<div id="alertaError" class="alert alert-danger alert-dismissible text-center d-none">
			  <strong>¡Error!</strong> <span>Faltan datos por llenar.</span>
			</div>
			<div class="form-inline mx-auto text-center d-block w-100">
				<div class="form-group">
					<div class="input-title">Cédula</div>
					<input id="cedula" class="form-control w-75" type="number"/>
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Nombres</div>
					<input id="nombres" class="form-control w-75" />
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Apellidos</div>
					<input id="apellidos" class="form-control w-75" />
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Fecha de nacimiento</div>
					<input id="fecha_nacimiento" class="form-control w-75" name="fecha" autocomplete="off" readonly />
				</div>
			</div>
		`,
		showCancelButton: true,
		confirmButtonText: 'Guardar',
		showLoaderOnConfirm: true,
		preConfirm: ()=>{
			return new Promise(function(resolve){

				var cedula = $('#cedula').val().trim();
				var nombres = $('#nombres').val().trim();
				var apellidos = $('#apellidos').val().trim();
				var fecha_nacimiento = $('#fecha_nacimiento').val().trim();
				var patron = /^[a-zA-ZÀ-ú\s]+$/;
				var soloLetras = new RegExp(patron);

				if(cedula == '' || nombres == '' || apellidos == '' || fecha_nacimiento == ''){
					$('#alertaError span').text('Faltan datos por llenar.');
					$('#alertaError').removeClass('d-none');
					$('input').each((i)=>{
						if($('input').get(i).value == ''){
							$($('input').get(i)).css('border', '1px solid red');
							$($('input').get(i)).on('change', function(){
								if($('input').get(i).value != ''){
									$($('input').get(i)).css('border', '');
								}else{
									$($('input').get(i)).css('border', '1px solid red');
									todosLlenos = false;
								}
							});
						}
					})
					Swal.disableLoading();
					return;
				}
				if(!soloLetras.test(nombres)){
					$('#alertaError span').text('El nombre contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#nombres').css('border', '1px solid red');
					$('#nombres').on('change', function(){
						if($(this).val() != '' && soloLetras.test($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}
				if(!soloLetras.test(apellidos)){
					$('#alertaError span').text('El apellido contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#apellidos').css('border', '1px solid red');
					$('#apellidos').on('change', function(){
						if($(this).val() != '' && soloLetras.test($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}

				var datos = {cedula : cedula, nombres : nombres, apellidos : apellidos, fecha_nacimiento : moment(fecha_nacimiento, 'DD/MM/YYYY').format('YYYY-MM-DD')};
				$.post(SERVERURL + '/estudiantes/guardar', datos, function(res){
					if(res == 'ok'){
						alerta.fire({
							title: '¡Estudiante Agregado!'
						})
						.then(()=>{
							window.location.reload();
						})
					}else{
						alerta.fire({
							title: '¡No se pudo agregar el estudiante!'
						})
					}
				});

			});
		}
		})
		calendario();
	})


	//Editar estudiante
	$('.editarEstudiante').click(function(){
		var boton = $(this);
		var cedulaOriginal = $(boton).parent().parent().find('.cedula').text().trim();
		var nombres = $(boton).parent().parent().find('.nombres').text().trim();
		var apellidos = $(boton).parent().parent().find('.apellidos').text().trim();
		var fecha_nacimiento = $(boton).parent().parent().find('.fecha_nacimiento').text().trim();
		alerta.fire({
		title: 'Editar estudiante',
		html: `
			<div id="alertaError" class="alert alert-danger text-center d-none">
			  <strong>¡Error!</strong> <span>Faltan datos por llenar.</span>
			</div>
			<div class="form-inline mx-auto text-center d-block w-100">
				<div class="form-group">
					<div class="input-title">Cédula</div>
					<input id="cedula" class="form-control w-75" type="number"/>
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Nombres</div>
					<input id="nombres" class="form-control w-75" />
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Apellidos</div>
					<input id="apellidos" class="form-control w-75" />
				</div>
				<div class="form-group mt-1">
					<div class="input-title">Fecha de nacimiento</div>
					<input id="fecha_nacimiento" class="form-control w-75" name="fecha" readonly />
				</div>
			</div>
		`,
		showCancelButton: true,
		confirmButtonText: 'Guardar',
		showLoaderOnConfirm: true,
		preConfirm: ()=>{
			return new Promise(function(resolve){
				var cedulaModificar = $('#cedula').val().trim();
				var nombresM = $('#nombres').val().trim();
				var apellidosM = $('#apellidos').val().trim();
				var fecha_nacimientoM = $('#fecha_nacimiento').val().trim();
				var patron = /^[a-zA-ZÀ-ú\s]+$/;
				var soloLetras = new RegExp(patron);

				if(cedulaModificar == '' || nombresM == '' || apellidosM == '' || fecha_nacimientoM== ''){
					$('#alertaError span').text('Faltan datos por llenar.');
					$('#alertaError').removeClass('d-none');
					$('input').each((i)=>{
						if($('input').get(i).value == ''){
							$($('input').get(i)).css('border', '1px solid red');
							$($('input').get(i)).on('change', function(){
								if($('input').get(i).value != ''){
									$($('input').get(i)).css('border', '');
								}else{
									$($('input').get(i)).css('border', '1px solid red');
									todosLlenos = false;
								}
							});
						}
					})
					Swal.disableLoading();
					return;
				}
				if(cedulaModificar == cedulaOriginal && nombres ==  nombresM && apellidos == apellidosM && fecha_nacimiento == fecha_nacimientoM){
					$('#alertaError span').text('No se cambió ningún dato.');
					$('#alertaError').removeClass('d-none');
					Swal.disableLoading();
					return;
				}
				if(!soloLetras.test(nombresM)){
					$('#alertaError span').text('El nombre contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#nombres').css('border', '1px solid red');
					$('#nombres').on('change', function(){
						if($(this).val() != '' && soloLetras.test($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}
				if(!soloLetras.test(apellidosM)){
					$('#alertaError span').text('El apellido contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#apellidos').css('border', '1px solid red');
					$('#apellidos').on('change', function(){
						if($(this).val() != '' && soloLetras.test($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}
				if(!soloLetras.test(nombresM)){
					$('#alertaError span').text('El nombre contiene carácteres incorrectos.');
					$('#alertaError').removeClass('d-none');
					$('#nombres').css('border', '1px solid red');
					$('#nombres').on('change', function(){
						if($(this).val() != '' && soloLetras.test($(this).val())){
							$(this).css('border','');
						}
					})
					Swal.disableLoading();
					return;
				}

				var datos = {cedulaOriginal : cedulaOriginal, cedulaModificar : cedulaModificar, nombres : nombresM, apellidos : apellidosM, fecha_nacimiento : moment(fecha_nacimientoM, 'DD/MM/YYYY').format('YYYY-MM-DD')};

				$.post(SERVERURL + '/estudiantes/editar', datos, function(res){
					if(res == 'ok'){
						alerta.fire({
							title: '¡Estudiante Editado!'
						})
						.then(()=>{
							window.location.reload();
						})
					}else{
						alerta.fire({
							title: '¡No se pudo editar el estudiante!'
						})
					}
				});

			});
		}
		})
		$('#cedula').val(cedulaOriginal);
		$('#nombres').val(nombres);
		$('#apellidos').val(apellidos);
		$('#fecha_nacimiento').val(fecha_nacimiento);
		calendario();
	});


	
	//Borrar estudiante
	$('.borrarEstudiante').click(function(){
		var cedula = $(this).parent().parent().find('.cedula').text().trim();
		var nombres = $(this).parent().parent().find('.nombres').text().trim();
		var apellidos = $(this).parent().parent().find('.apellidos').text().trim();
		alerta.fire({
			title: `¿Seguro de eliminar el estudiante ${nombres} ${apellidos}?`,
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					$.post(SERVERURL + '/estudiantes/borrar', {cedula : cedula}, function(res){
						if(res == 'ok'){
						alerta.fire({
							title: '¡Estudiante Borrado!'
						})
						.then(()=>{
							window.location.reload();
						})
						}else{
							alerta.fire({
								title: '¡No se pudo borrar el profesor!'
							})
						}
					});
				});
			}
		})
	});


	//Agregar materia
	$('.agregarMateria').click(function(){
		alerta.fire({
			title: 'Agregar materia',
			html: `
				<div id="alertaError" class="alert alert-danger text-center d-none">
			  		<strong>¡Error!</strong> <span>Faltan datos por llenar.</span>
				</div>
				<div class="form-inline mx-auto text-center d-block w-100">
					<div class="form-group">
						<div class="input-title">Nombre</div>
						<input id="nombre" class="form-control w-75" type="text"/>
					</div>
				</div>
			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					var nombre = $('#nombre').val().trim();
					if(nombre == ''){
						$('#alertaError').removeClass('d-none');
						Swal.disableLoading();
						return;
					}
					$.post(SERVERURL + '/materias/agregar', {nombre : nombre}, function(res){

						if(res == 'ok'){
							alerta.fire({
								title: '¡Materia agregada!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							alerta.fire({
								title: '¡No se pudo agregar la materia!'
							})
						}

					});

				});
			}
		});
	});


	//Editar materia
	$('.editarMateria').click(function(){
		var idMateria = $(this).parent().parent().find('.idMateria').val().trim();
		var nombre = $(this).parent().parent().find('.nombre').text().trim();

		alerta.fire({
			title: 'Editar materia',
			html: `
				<div id="alertaError" class="alert alert-danger text-center d-none">
			  		<strong>¡Error!</strong> <span>Faltan datos por llenar.</span>
				</div>
				<div class="form-inline mx-auto text-center d-block w-100">
					<div class="form-group">
						<div class="input-title">Nombre</div>
						<input id="nombre" class="form-control w-75" type="text"/>
					</div>
				</div>
			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					var nombreM = $('#nombre').val().trim();
					if(nombre == ''){
						$('#alertaError').removeClass('d-none');
						Swal.disableLoading();
						return;
					}
					if(nombre == nombreM){
						$('#alertaError').text('No se ha cambiado ningún dato.');
						$('#alertaError').removeClass('d-none');
						Swal.disableLoading();
						return;
					}
					$.post(SERVERURL + '/materias/editar', {idMateria : idMateria, nombre : nombreM}, function(res){

						if(res == 'ok'){
							alerta.fire({
								title: '¡Materia editada!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							alerta.fire({
								title: '¡No se pudo editar la materia!'
							})
						}

					});

				});
			}
		})

		$('#nombre').val(nombre);

	});


	//Borrar materia
	$('.borrarMateria').click(function(){
		var idMateria = $(this).parent().parent().find('.idMateria').val().trim();
		var nombre = $(this).parent().parent().find('.nombre').text().trim();

		alerta.fire({
			title: `¿Seguro de borrar la materia ${nombre}?`,
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					$.post(SERVERURL + '/materias/borrar', {idMateria : idMateria}, function(res){

						if(res == 'ok'){
							alerta.fire({
								title: '¡Materia borrada!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							alerta.fire({
								title: '¡No se pudo borrar la materia!'
							})
						}

					});

				});
			}
		})
	});



	//Ver secciones
	$('.verSecciones').click(function(){

		var grado = $(this).parent().parent().find('.nombre').text().trim();
		var id = $(this).parent().parent().find('.idgrado').val().trim();

		alerta.fire({
			title: 'Secciones de ' + grado,
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`,
		})
		
		$.post(SERVERURL + '/seccion/getSeccionPorGrado', {id : id}, function(res){
			var secciones = [];
			res = JSON.parse(res);
			var tabla = '<table class="table table-hover table-stripped">';
			if(res == 'no' || res.length < 1){
				tabla += `<tr><td>No hay secciones agregadas.</td></tr>`;
			}else{
				tabla += `
						<th>numero</th>
						<th>acciones</th>
				 `;
				res.forEach((r)=>{
					tabla += `
							<tr>
								<td class="link verSeccionDeGrado" id_grado="${r.id_grado}" numero="${r.numero}" grado="${grado}">${r.numero}</td>
								<td>
									<img class="borrar borrarSeccionDeGrado" src="${SERVERURL}/views/img/borrar.svg" height="25" width="25" idgrado="${id}" numero="${r.numero}" nombreGrado="${grado}" />
								</td>
							<tr>
					`;
					secciones.push(r.numero);
				});
			}
			tabla += '</tabla>';
			$('#contenido').html(tabla);
			secciones = JSON.stringify(secciones);
			$('#contenido').append(`<button idgrado="${id}" secciones='${secciones}' class="btn btn-success d-block mx-auto mt-4 agregarSeccion" type="button">Agregar</button>`);
		});

	});


	//Agregar seccion
	$(document).on('click', '.agregarSeccion', function(){

		var idGrado = $(this).attr('idgrado');
		var secciones = JSON.parse($(this).attr('secciones'));

		var secc = `<select id="seccionesAgregar" class="form-control w-25 mx-auto d-block" placeholder="Número...">`;

		for(i=1; i<secciones.length+2; i++){
			if(!secciones.includes(i)){
				secc += `<option value="${i}">${i}</option>`;
			}
		}
		secc += '</select>';
		
		alerta.fire({
			title: 'Agregar sección',
			html: secc,
			showCancelButton: true,
			confirmButtonText: 'Agregar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					var numero = $('#seccionesAgregar').val().trim();

					$.post(SERVERURL + '/seccion/agregarAGrado', {idGrado : idGrado, numero : numero}, function(res){

						if(res == 'ok'){
							alerta.fire({
								title: '¡Sección agregada!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							alerta.fire({
								title: '¡No se pudo agregar la sección!'
							})
						}

					});

				});
			}
		});

	});



	//Borrar seccion de grado
	$(document).on('click', '.borrarSeccionDeGrado', function(res){
		var idGrado = $(this).attr('idgrado');
		var numero = $(this).attr('numero');
		var nombreGrado = $(this).attr('nombreGrado');
		alerta.fire({
			title: `¿Seguro de borrar la sección número ${numero} de ${nombreGrado}?`,
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					$.post(SERVERURL + '/seccion/borrarSeccionDeGrado', {idGrado : idGrado, numero : numero}, function(res){

						if(res == 'ok'){
							alerta.fire({
								title: '¡Sección borrada!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							alerta.fire({
								title: '¡No se pudo borrar la sección!'
							})
						}

					});

				});
			}
		})

	});


	//Agregar materia a grado
	$(document).on('click', '.gradoMateria', function(){
		var idMateria = $(this).parent().parent().find('.idMateria').val();
		if(idMateria != undefined){
			idMateria = idMateria.trim();
			var nombre = $(this).parent().parent().find('.nombre').text().trim();
		}else{
			var idMateria = $(this).attr('idMateria').trim();
			var nombre = $(this).attr('nombre').trim();
		}
		
		$.post(SERVERURL + '/materias/getGradosNotIn', {idMateria : idMateria}, function(res){

			if(res != 'no' && res.length > 0){
				res = JSON.parse(res);
				var tabla = '<select id="gradoAgregar" class="form-control">'
				res.forEach((r)=>{
					tabla += `
						<option value="${r.id}">${r.nombre}</option>`;
				});
				tabla += '</select>';
				$('#contenido').html(tabla);
			}
			

		});
		alerta.fire({
			title: 'Agregar grado a materia ' + nombre,
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(res){
					var idGrado = $('#gradoAgregar').val().trim();
					$.post(SERVERURL + '/materias/agregarGrado', {idGrado : idGrado, idMateria : idMateria}, function(res){

						if(res == 'ok'){
							alerta.fire({
								title: '¡Grado agregado!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							alerta.fire({
								title: '¡No se pudo agregar el grado!'
							})
						}

					})
				});
			}
		})
	})


	//Ver grado de materia
	$('.verGrado').click(function(){
		var idMateria = $(this).parent().parent().find('.idMateria').val().trim();
		var nombre = $(this).parent().parent().find('.nombre').text().trim();
		$.post(SERVERURL + '/materias/getGradosPorIdMateria', {idMateria : idMateria}, function(res){

			res = JSON.parse(res);
			if(res.length > 0){
				var tabla = '<table class="table table-hover table-stripped">';
				tabla += `<th>Nombre</th>
						  <th>Accion</th>`;
				res.forEach((r)=>{
					tabla += `<tr><td>${r.nombre}</td><td><img class="borrar borrarGradoDeMateria" src="${SERVERURL}/views/img/borrar.svg" height="25" width="25" idMateria=${idMateria} nombre=${nombre} idGrado=${r.id_grado} /></td></tr>`;
				})
				tabla += '</table>';
			}else{
				tabla = '<h4 class="text-center">No hay grados agregados.</h4>'
			}
			tabla += `<button idMateria="${idMateria}" nombre="${nombre}" class="btn btn-success d-block mx-auto mt-5 gradoMateria" type="button">Agregar</button>`;
			
			$('#contenido').html(tabla);
		});
		alerta.fire({
			title: 'Grados de ' + nombre,
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`
		});
	});


	//Borrar grado de materia
	$(document).on('click', '.borrarGradoDeMateria', function(){

		var idMateria = $(this).attr('idMateria').trim();
		var idGrado = $(this).attr('idGrado').trim();

		alerta.fire({
			title: '¿Seguro que desea borrar el grado?',
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					$.post(SERVERURL + '/materias/borrarGradoDeMateria', {idMateria : idMateria, idGrado : idGrado}, function(res){
						if(res == 'ok'){
							alerta.fire({
								title: '¡Grado borrado!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							alerta.fire({
								title: '¡No se pudo borrar el grado!'
							})
						}
					});
				});
			}
		})

		
	});


	//Ver grado de estudiante
	$('.verGradoDeEstudiante').on('click', function(){
		var cedula = $(this).attr('cedula').trim();
		var id_grado = $(this).attr('id_grado').trim();
		var nombre = $(this).attr('nombre').trim();
		var nombreGrado = $(this).text().trim();
		var seccion = '';
		var numero = $(this).attr('numero');

		alerta.fire({
			title: nombre,
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`,
		})

		if(id_grado == '0'){
			$('#contenido').html(`<h4 class="text-center">No inscrito.</h4><br><button class="btn btn-success d-block mx-auto inscribirEstudianteAGrado" cedula="${cedula}" nombre="${nombre}">Inscribir</button>`);
		}else{

			$.post(SERVERURL + '/estudiantes/getCantidadDeMaterias', {cedula:cedula}, function(res){

				res = JSON.parse(res);

				if(id_grado == '1'){
					tabla = `

						<table class="table table-hover">
							<th>Grado</th>
							<th>Sección</th>
							<th>Materias</th>
							<th>Accion</th>
							<tr cedula="${cedula}" nombre="${nombre}">
								<td>${nombreGrado}</td>
								<td>${numero}</td>
								<td class="link verMateriasDeEstudiante" id_grado="${id_grado}">${res.materias}</td>
								<td><img class="borrar borrarGradoDeEstudiante" src="${SERVERURL}/views/img/borrar.svg" height="25" width="25" id_grado="${id_grado}" nombre="${nombre}" cedula="${cedula}" nombreGrado="${nombreGrado}" /></td>
							</tr>
						</table>


					`
				}else{
					tabla = `

						<table class="table table-hover">
							<th>Grado</th>
							<th>Sección</th>
							<th>Materias</th>
							<th>Arrastre</th>
							<th>Accion</th>
							<tr cedula="${cedula}" nombre="${nombre}">
								<td>${nombreGrado}</td>
								<td>${numero}</td>
								<td class="link verMateriasDeEstudiante" id_grado="${id_grado}">${res.materias}</td>
								<td class="link verMateriasDeArrastreDeEstudiante" id_grado="${id_grado}">${res.arrastre}</td>
								<td><img class="borrar borrarGradoDeEstudiante" src="${SERVERURL}/views/img/borrar.svg" height="25" width="25" id_grado="${id_grado}" nombre="${nombre}" cedula="${cedula}" nombreGrado="${nombreGrado}" /></td>
							</tr>
						</table>

				`
				}

				$('#contenido').html(tabla);

			});

		}
		
	});


	//Inscribir estudiante a grado
	$(document).on('click', '.inscribirEstudianteAGrado', function(){

		var cedula = $(this).attr('cedula').trim();
		var nombre = $(this).attr('nombre').trim();

		alerta.fire({
			title: `Inscribir al estudiante<br> ${nombre}`,
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`,
		})

		$.post(SERVERURL + '/grados/getTodos', function(res){

			res = JSON.parse(res);
			var tabla = `<table class="table table-hover table-stripped" >`;
			res.forEach((r)=>{
				tabla += `<tr class="link inscribirEstudianteAGradoClick" id_grado="${r.id}">
							<td>${r.nombre}</td>
						  </tr>
				`;
			});
			tabla += '</table>';

			$('#contenido').html(tabla);


			$('.inscribirEstudianteAGradoClick').click(function(){
				var id_grado = $(this).attr('id_grado').trim();
				alerta.fire({
					title: `Inscribir al estudiante<br> ${nombre}`,
					html: `
						<div id="contenido">
							<div class="spinner-border text-primary"></div>
						</div>
					`,
					showCancelButton: true

				})
				.then(()=>{


					alerta.fire({
					title: `Inscribir al estudiante<br> ${nombre}`,
					html: `
						<div id="contenido">
							<div class="spinner-border text-primary"></div>
						</div>
					`,
					showCancelButton: true,
					confirmButtonText: 'Guardar',
					showLoaderOnConfirm: true,
					preConfirm: ()=>{
						return new Promise(function(res){

							$.post(SERVERURL + '/estudiantes/inscribirEstudianteAGrado', {id_grado : id_grado, cedula : cedula, seccion : seccion}, function(res){

								if(res == 'ok'){
									
									var materias = [];
									var materiasArrastre = [];
									var periodo = $('#periodo').val();

									$('#materias').find('input[type=checkbox]').each((i)=>{

										var checkbox = $('#materias').find('input[type=checkbox]').get(i);
										if(checkbox.checked){
											materias.push(checkbox.getAttribute('id_materia'));
										}

									});

									$('#materiasArrastre').find('input[type=checkbox]').each((i)=>{

										var checkbox = $('#materiasArrastre').find('input[type=checkbox]').get(i);
										if(checkbox.checked){
											materiasArrastre.push(checkbox.getAttribute('id_materia'));
										}

									});

									var materias = JSON.stringify(materias);
									var materiasArrastre = JSON.stringify(materiasArrastre);
									$.post(SERVERURL + '/estudiantes/agregarMaterias', {materias : materias, materiasArrastre : materiasArrastre, id_grado : id_grado, cedula : cedula, periodo : periodo}, function(res){

										if(res == 'ok'){
											alerta.fire({
												title: '¡Grado y materias agregadas!'
											})
											.then(()=>{
												window.location.reload();
											})
										}else{
											alerta.fire({
												title: '¡No se pudo agregar las materias!'
											})
											.then(()=>{
												window.location.reload();
											})
										}

									});

								}else{
									alerta.fire({
										title: '¡Error al inscribir estudiante!'
									})
								}

							});

						});
					}
				})

				$('.swal2-confirm').attr('disabled', 'disabled');

				$.post(SERVERURL + '/materias/getMateriasPorGrado', {id_grado : id_grado}, function(res){
					if(typeof seccion == 'undefined' || seccion == ''){
						Swal.close();
						return;
					}
					$('.swal2-confirm').removeAttr('disabled');
					var tabla = `
						<select id="periodo" class="form-control mb-2 text-center d-block mx-auto" style="width:140px">
							<option value="${moment().format('YYYY')}">${moment().format('YYYY')} - ${parseInt(moment().format('YYYY'))+1}</option>
							<option value="${parseInt(moment().format('YYYY'))+1}">${parseInt(moment().format('YYYY'))+1} - ${parseInt(moment().format('YYYY'))+2}</option>
						</select>
					`;
					tabla += `<table id="materias" class="table table-hover table-stripped text-center"><th>Nombre</th><th>Seleccionar</th>`;
					res = JSON.parse(res);
					if(res == 'no' || res.length < 1){
						tabla += `<tr class="tex-center"><td colspan="2">No hay materias en este grado.</td></tr>`;
					}else{
						res.forEach((r)=>{
							tabla += `<tr>
									  	<td>${r.nombre}</td>
									  	<td><input id_materia="${r.id}" type="checkbox" checked/></td>
								  	  </tr>
								   `;
						});
						//Consultar materias del grado anterior por si arrastra
						if(id_grado > 1){
							var idgrado = parseInt(id_grado)-1;
							$.post(SERVERURL + '/materias/getMateriasPorGrado', {id_grado : idgrado}, function(res){
								console.log(res);
								res = JSON.parse(res);
								var tablaReparacion = `<table id="materiasArrastre" class="table table-hover table-stripped text-center"><th>Nombre</th><th>Seleccionar</th>`;
								if(!res == 'no' || !res.length < 1){
									res.forEach((r)=>{
										tablaReparacion += `
												  <tr>
												  	<td>${r.nombre}</td>
												  	<td><input id_materia="${r.id}" type="checkbox"/></td>
											  	  </tr>
											   `;
									});
								}
								tablaReparacion += '</table>';
								tabla += '</table>';
								$('#contenido').html(tabla + '<h4 class="text-center">Materias de arrastre</h4>' + tablaReparacion);
							});
						}else{
							tabla += '</table>';
							$('#contenido').html(tabla);
						}

						
					}
					
				})


				})

				$('.swal2-confirm').attr('disabled', 'disabled');
				$.post(SERVERURL + '/seccion/getSeccionPorGrado', {id : id_grado}, function(res){

					res = JSON.parse(res);
					var tabla = '<table class="table table-hover table-stripped"><th>Sección</th>';
					if(res.length < 1){
						tabla += `
							<tr>
								<td colspan="2">No hay secciones disponibles.</td>
							</tr>
						`;
					}
					res.forEach((r)=>{
						tabla += `
								<tr class="link seccionGrado" id_grado="${r.id_grado}" numero="${r.numero}">
									<td>${r.numero}</td>
								</tr>
						`;
					});
					tabla += '</table>';
					$('#contenido').html(tabla);
					
					$('.seccionGrado').click(function(){

						seccion = $(this).attr('numero');
						$('.swal2-confirm').removeAttr('disabled');
						$('.swal2-confirm').trigger('click');

					});

				});

				
				
			});


		});


	});

	//Borrar grado de estudiante
	$(document).on('click', '.borrarGradoDeEstudiante', function(){

		var nombre = $(this).attr('nombre').trim();
		var nombregrado = $(this).attr('nombregrado').trim();
		var id_grado = $(this).attr('id_grado').trim();
		var cedula = $(this).attr('cedula').trim();

		alerta.fire({
			title: '¿Seguro de borrar el grado ' + nombregrado + ' del estudiante ' + nombre + '?',
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					$.post(SERVERURL + '/estudiantes/borrarGradoDeEstudiante', {cedula:cedula}, function(res){
						if(res == 'ok'){
							alerta.fire({
								title: '¡Grado del estudiante borrado!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							alerta.fire({
								title: '¡No se pudo borrar el grado!'
							})
						}
					});
				});
			}
		})

	});


	//Ver materias de estudiante
	$(document).on('click', '.verMateriasDeEstudiante', function(res){

		var nombre = $(this).parent().attr('nombre');
		var cedula = $(this).parent().attr('cedula');
		var id_grado = $(this).attr('id_grado');

		alerta.fire({
			title: 'Materias del estudiante<br>' + nombre,
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`
		})

		$.post(SERVERURL + '/estudiantes/getMaterias', {cedula:cedula}, function(res){
			res = JSON.parse(res);
			var tabla = `<table class="table table-hover table-stripped">
							<th>Nombre</th>
							<th>Accion</th>`;
			res.forEach((r)=>{
				tabla += `
					<tr>
						<td>${r.nombre}</td>
						<td>
							<img class="borrar borrarMateriaDeEstudiante" src="${SERVERURL}/views/img/borrar.svg" height="25" width="25" id_materia="${r.id}" id_grado="${r.id_grado}" cedula="${cedula}"/>
						</td>
					</tr>
				`;
			});
			tabla += '</table>';
			$('#contenido').html(tabla + `<button class="btn btn-success agregarMateriaAEstudiante" cedula="${cedula}" nombre="${nombre}" id_grado="${id_grado}">Agregar</button>`);
		})

	});



	//Ver materias de arrastre estudiante
	$(document).on('click', '.verMateriasDeArrastreDeEstudiante', function(res){

		var nombre = $(this).parent().attr('nombre');
		var cedula = $(this).parent().attr('cedula');
		var id_grado = $(this).attr('id_grado');
		id_grado = parseInt(id_grado)-1;
		alerta.fire({
			title: 'Materias de arrastre del estudiante<br>' + nombre,
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`
		})

		$.post(SERVERURL + '/estudiantes/getMateriasDeArrastre', {cedula:cedula}, function(res){
			res = JSON.parse(res);
			var tabla = `<table class="table table-hover table-stripped">
							<th>Nombre</th>
							<th>Accion</th>`;
			res.forEach((r)=>{
				tabla += `
					<tr>
						<td>${r.nombre}</td>
						<td>
							<img class="borrar borrarMateriaDeEstudiante" src="${SERVERURL}/views/img/borrar.svg" height="25" width="25" id_materia="${r.id}" id_grado="${r.id_grado}" cedula="${cedula}"/>
						</td>
					</tr>
				`;
			});
			tabla += '</table>';
			$('#contenido').html(tabla + `<button class="btn btn-success agregarMateriaDeArrastreAEstudiante" cedula="${cedula}" nombre="${nombre}" id_grado="${id_grado}">Agregar</button>`);
		})

	});


	//Borrar materia de estudiante
	$(document).on('click', '.borrarMateriaDeEstudiante', function(){

		var id_materia = $(this).attr('id_materia').trim();
		var id_grado = $(this).attr('id_grado').trim();
		var cedula = $(this).attr('cedula').trim();

		alerta.fire({
			title: '¿Seguro de borrar la materia?',
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					$.post(SERVERURL + '/estudiantes/borrarMateriaDeEstudiante', {id_materia : id_materia, id_grado : id_grado, cedula : cedula}, function(res){
					if(res == 'ok'){
						alerta.fire({
							title: '¡Materia borrada del estudiante!'
						})
						.then(()=>{
							window.location.reload();
						})
					}else{
						alerta.fire({
							title: '¡No se pudo borrar la materia!'
						})
					}
				});
				})
			}
		})

	})

	//Agregar materia a estudiante
	$(document).on('click', '.agregarMateriaAEstudiante', function(){
		var cedula = $(this).attr('cedula').trim();
		var nombre = $(this).attr('nombre').trim();
		var id_grado = $(this).attr('id_grado').trim();
		alerta.fire({
			title: 'Agregar materia al estudiante<br>' + nombre,
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`
		})

		$.post(SERVERURL + '/estudiantes/getMateriasNotIn', {cedula:cedula,id_grado:id_grado}, function(res){
			res = JSON.parse(res);
			var tabla = `<table id="agregarMateriasTable" class="table table-hover table-stripped">
							<th>Nombre</th>
							<th>Agregar</th>`;
			if(typeof res == 'object'){
				if(res.length < 1){
					tabla += `
						<tr>
							<td colspan="2">No hay materias para agregar.</td>
						</tr>
					`;
				}
				res.forEach((r)=>{
					tabla += `
						<tr>
							<td>${r.nombre}</td>
							<td><input type="checkbox" id_materia="${r.id}" id_grado="${r.id_grado}"/></td>
						</tr>
					`;
					});
			}else{
				tabla += `
					<tr class="text-center">
						<td colspan="2">No hay materias por agregar.</td>
					</tr>
				`;
			}
			
			tabla += '</table>';
			$('#contenido').html(tabla + `<button class="btn btn-success guardarAgregarMateriaEstudiante" cedula="${cedula}" id_grado="${id_grado}">Agregar</button>`);
		})

	})

	//Guardar agregar materia a estudiante
	$(document).on('click', '.guardarAgregarMateriaEstudiante', function(){
		var cedula = $(this).attr('cedula').trim();
		var id_grado = $(this).attr('id_grado').trim();
		$(this).attr('disabled', 'disabled');
		var materias = [];
		$('#agregarMateriasTable').find('input[type=checkbox]').each((i)=>{

			var checkbox = $('#agregarMateriasTable').find('input[type=checkbox]').get(i);
			if(checkbox.checked){
				materias.push(checkbox.getAttribute('id_materia'));
			}

		})
		if(materias.length < 1){
			$(this).removeAttr('disabled');
			return;
		}

		materias = JSON.stringify(materias);
		var materiasArrastre = [];
		materiasArrastre = JSON.stringify(materiasArrastre);

		$.post(SERVERURL + '/estudiantes/agregarMaterias', {materias:materias,materiasArrastre:materiasArrastre,cedula:cedula,id_grado:id_grado}, function(res){
			if(res == 'ok'){
				alerta.fire({
				title: '¡Materia/s agregada/s!'
				})
				.then(()=>{
					window.location.reload();
				})
			}else{
				alerta.fire({
					title: '¡No se pudo agregar la/s materia/s!'
				})
			}
		});

	});


	//Agregar materia de arrastre a estudiante
	$(document).on('click', '.agregarMateriaDeArrastreAEstudiante', function(){
		var cedula = $(this).attr('cedula').trim();
		var nombre = $(this).attr('nombre').trim();
		var id_grado = $(this).attr('id_grado').trim();
		alerta.fire({
			title: 'Agregar materia de arrastre al estudiante<br>' + nombre,
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`
		})

		$.post(SERVERURL + '/estudiantes/getMateriasDeArrastreNotIn', {cedula:cedula,id_grado:id_grado}, function(res){
			res = JSON.parse(res);
			var tabla = `<table id="agregarMateriasTable" class="table table-hover table-stripped">
							<th>Nombre</th>
							<th>Agregar</th>`;
			if(typeof res == 'object'){
				if(res.length < 1){
					tabla += `
						<tr>
							<td colspan="2">No hay materias para agregar.</td>
						</tr>
					`;
				}
				res.forEach((r)=>{
					tabla += `
						<tr>
							<td>${r.nombre}</td>
							<td><input type="checkbox" id_materia="${r.id}" id_grado="${r.id_grado}"/></td>
						</tr>
					`;
					});
			}else{
				tabla += `
					<tr class="text-center">
						<td colspan="2">No hay materias por agregar.</td>
					</tr>
				`;
			}
			
			tabla += '</table>';
			$('#contenido').html(tabla + `<button class="btn btn-success guardarAgregarMateriaDeArrastreAEstudiante" cedula="${cedula}" id_grado="${id_grado}">Agregar</button>`);
		})

	})



	//Guardar agregar materia de arrastre a estudiante
	$(document).on('click', '.guardarAgregarMateriaDeArrastreAEstudiante', function(){
		var cedula = $(this).attr('cedula').trim();
		var id_grado = $(this).attr('id_grado').trim();
		$(this).attr('disabled', 'disabled');
		var materias = [];
		$('#agregarMateriasTable').find('input[type=checkbox]').each((i)=>{

			var checkbox = $('#agregarMateriasTable').find('input[type=checkbox]').get(i);
			if(checkbox.checked){
				materias.push(checkbox.getAttribute('id_materia'));
			}

		})
		if(materias.length < 1){
			$(this).removeAttr('disabled');
			return;
		}

		materias = JSON.stringify(materias);
		var materiasArrastre = [];
		materiasArrastre = JSON.stringify(materiasArrastre);

		$.post(SERVERURL + '/estudiantes/agregarMaterias', {materias:materias,materiasArrastre:materiasArrastre,cedula:cedula,id_grado:id_grado}, function(res){
			if(res == 'ok'){
				alerta.fire({
				title: '¡Materia/s agregada/s!'
				})
				.then(()=>{
					window.location.reload();
				})
			}else{
				alerta.fire({
					title: '¡No se pudo agregar la/s materia/s!'
				})
			}
		});

	});



	//Ver materias de profesor
	$(document).on('click', '.verMateriasProfesor', function(){

		var cedula = $(this).attr('cedula');
		var nombres = $(this).attr('nombres');
		var apellidos = $(this).attr('apellidos');
		var id_grado = '';
		var seccion = '';
		var cancelar = false;

		alerta.fire({
			title: 'Materias que dicta el profesor<br>' + nombres + ' ' + apellidos,
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`,
			allowOutsideClick: false,
			showCancelButton: true
		})
		.then(function(result){
			if(id_grado == ''){
				return;
			}
			if(result.value){

			}else{
				return;
			}
			alerta.fire({
			title: 'Agregar materia al profesor<br>' + nombres + ' ' + apellidos,
			html: `
				<div id="contenido">
						<div class="spinner-border text-primary"></div>
					</div>
				`,
				showCancelButton: true,
				allowOutsideClick: false,
				confirmButtonText: 'Agregar',
				showLoaderOnConfirm: true,
			})
			.then(()=>{
				var materias = [];
				$('#materiasTable').find('input[type=checkbox]').each((i)=>{
					var checkbox = $('#materiasTable').find('input[type=checkbox]').get(i);
					if(checkbox.checked){
						materias.push(checkbox.getAttribute('id_materia'));
					}
				});
				if(materias.length < 1){
					return;
				}
				materias = JSON.stringify(materias);
				if(seccion == '' || cancelar){
					return;
				}
				$.post(SERVERURL + '/profesores/agregarMaterias', {materias : materias, cedula : cedula, id_grado : id_grado, seccion : seccion}, function(res){

					if(res == 'ok'){
						alerta.fire({
						title: '¡Materia/s agregada/s!'
						})
						.then(()=>{
							window.location.reload();
						})
					}else{
						alerta.fire({
							title: '¡No se pudo agregar la/s materia/s!'
						})
					}

				});

			})

			$('.swal2-confirm').addClass('d-none');
			$.post(SERVERURL + '/seccion/getSeccionPorGrado', {id : id_grado}, function(res){
				res = JSON.parse(res);
				var tabla = '<table class="table table-hover table-stripped"><th>Seccion</th>';
				if(res.length < 1){
					tabla += '<tr class="text-center"><td>No hay secciones en este grado.</td></tr>';
				}else{
					res.forEach((r)=>{
						tabla += `
							<tr class="link agregarMateriaPorfesorSeccion" numero="${r.numero}">
								<td>${r.numero}</td>
							</tr>
						`;
					});
				}
				tabla += '</table>';
				$('#contenido').html(tabla);


				$('.agregarMateriaPorfesorSeccion').click(function(){
					seccion = $(this).attr('numero');
					$.post(SERVERURL + '/profesores/getMateriasPorGradoNotIn', {id_grado : id_grado, seccion : seccion, cedula : cedula}, function(res){
						console.log(res);
						res = JSON.parse(res);
						var tabla = '<table id="materiasTable" class="table table-hover table-stripped"><th>Materia</th><th>Agregar</th>';
						if(res.length < 1){
							tabla += '<tr class="text-center"><td colspan="2">No hay materias disponibles en este grado.</td></tr>';
						}else{
							res.forEach((r)=>{
								tabla += `
									<tr class="agregarMateriaPorfesorSeccion" numero="${r.numero}">
										<td>${r.nombre}</td>
										<td><input type="checkbox" id_materia="${r.id}" /></td>
									</tr>
								`;
							});
						}
						tabla += '</table>';
						$('#contenido').html(tabla);
						$('.swal2-confirm').removeClass('d-none');
						$(document).on('click', '.swal2-cancel', function(){
							cancelar = true;
						})
					});

				});


			});

		})
		$('.swal2-confirm').addClass('d-none');
		$.post(SERVERURL + '/profesores/getMaterias',{cedula:cedula},function(res){

			res = JSON.parse(res);

			var tabla = '<table class="table table-hover table-stripped"><th>Nombre</th><th>Grado</th><th>Seccion</th><th>Accion</th>';
			if(res.length < 1){
				tabla += '<tr class="text-center"><td colspan="4">No dicta ninguna materia.</td></tr>';
			}else{
				res.forEach((r)=>{
					tabla += `
						<tr>
							<td>${r.nombre_materia}</td>
							<td>${r.nombre_grado}</td>
							<td>${r.seccion_numero}</td>
							<td>									
								<img class="borrar borrarMateriaDeProfesor" src="${SERVERURL}/views/img/borrar.svg" height="25" width="25" id_grado="${r.id_grado}" numero="${r.seccion_numero}" cedula="${cedula}" id_materia="${r.id_materia}" />
							</td>
						</tr>
					`;
				});
			}
			tabla += '</table>';
			$('#contenido').html(tabla + `<button class="btn btn-success d-block mx-auto agregarMateriaAProfesor" cedula="${cedula}" nombres="${nombres}" apellidos="${apellidos}">Agregar</button>`);
		

			$('.agregarMateriaAProfesor').click(function(){
				$(this).attr('disabled','disabled');
				$.post(SERVERURL + '/grados/getTodos', function(res){
					res = JSON.parse(res);
					var tabla = '<table class="table table-hover table-stripped"><th>Nombre</th>';
					if(res.length < 1){
						tabla += '<tr class="text-center"><td>No hay grados registrados.</td></tr>';
					}else{
						res.forEach((r)=>{
							tabla += `
								<tr class="link agregarMateriaProfeClick" id_grado="${r.id}">
									<td>${r.nombre}</td>
								</tr>
							`;
						});
					}
					tabla += '</table>';
					$('.swal2-confirm').attr('disabled','disabled');
					$('.swal2-confirm').addClass('d-none');
					$('.swal2-title').text('Agregar materia al profesor ' + nombres + ' ' + apellidos);
					$('#contenido').html(tabla);


					$('.agregarMateriaProfeClick').click(function(){
						id_grado = $(this).attr('id_grado');
						$('.swal2-confirm').removeAttr('disabled');
						$('.swal2-confirm').trigger('click');
					});

				});
			});

		});

	});


	//Borrar materia de profesor
	$(document).on('click', '.borrarMateriaDeProfesor', function(){

		var cedula = $(this).attr('cedula');
		var id_grado = $(this).attr('id_grado');
		var id_materia = $(this).attr('id_materia');
		var numero = $(this).attr('numero');

		alerta.fire({
			title: '¿Seguro que desea borrar la materia del profesor?',
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					$.post(SERVERURL + '/profesores/borrarMateria', {cedula:cedula,id_grado:id_grado,id_materia:id_materia,numero:numero}, function(res){
						if(res == 'ok'){
							alerta.fire({
								title: '¡Materia borrada!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							alerta.fire({
								title: '¡No se pudo borrar la materia!'
							})
						}
					});
				});
			}
		})

	});


	//Ver seccion de grado
	$(document).on('click', '.verSeccionDeGrado', function(){
		var id_grado = $(this).attr('id_grado');
		var numero = $(this).attr('numero');
		var grado = $(this).attr('grado');
		alerta.fire({
			title: 'Sección ' + numero +  ' de ' + grado,
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`
		})

		$.post(SERVERURL + '/estudiantes/getEstudiantesPorGradoYSeccion', {id_grado : id_grado, numero : numero}, function(res){
			console.log(res);
			res = JSON.parse(res);
			var tabla = '<table class="table table-hover table-stripped"><th>Ci</th><th>Nombres</th><th>Apellidos</th><th>Accion</th>';
			if(res.length < 1){
				tabla += '<tr class="text-center"><td colspan="4">No hay estudiantes registrados.</td></tr>';
			}else{
				res.forEach((r)=>{
					tabla += `
						<tr>
							<td>${r.cedula}</td>
							<td>${r.nombres}</td>
							<td>${r.apellidos}</td>
							<td>
								<img class="borrar borrarGradoDeEstudiante" src="${SERVERURL}/views/img/borrar.svg" height="25" width="25" id_grado="${id_grado}" nombre="${r.nombres} ${r.apellidos}" cedula="${r.cedula}" nombreGrado="${grado}" />
							</td>
						</tr>
					`;
				});
			}
			tabla += '</table>';
			$('#contenido').html(tabla);
		});
	});



	//Guardar notas de estudiante
	$('.guardarNotasEstudiante').click(function(){

		alerta.fire({
			title: '¿Seguro de guardar las notas?',
			showCancelButton: true,
			showLoaderOnConfirm: true,
			confirmButtonText: 'Guardar',
			preConfirm: ()=>{
				return new Promise(function(resolve){

					var calificaciones = [];
					var error = false;
					$('#tablaCalificaciones').find('.rowTabla').each((i)=>{
						var tr = $('#tablaCalificaciones').find('.rowTabla').get(i);
						
						$(tr).find('input').each((i)=>{
							var input = $(tr).find('input').get(i);
							if(input.value > 20 || input.value < 0){
								$(input).css('border', '1px solid red');
								$(input).on('change', function(){
									if($(this).val() <= 20 && $(this).val() >= 0){
										$(this).css('border', '');
									}
								});
								error = true;
							}
						});

						var id_materia = tr.getAttribute('id_materia');
						var id_grado = tr.getAttribute('id_grado');
						var cedula = tr.getAttribute('cedula');
						var lapso1 = $(tr).find('#lapso1').val();
						var lapso2 = $(tr).find('#lapso2').val();
						var lapso3 = $(tr).find('#lapso3').val();
						var recu1 = $(tr).find('#recu1').val();
						var recu2 = $(tr).find('#recu2').val();

						var obj = {id_grado : id_grado, id_materia : id_materia, cedula : cedula, lapso1 : lapso1, lapso2: lapso2, lapso3 : lapso3, recu1 : recu1, recu2 : recu2};
						calificaciones.push(obj);
					});

					if(error){
						alerta.fire({
							title: '¡Una o más notas son incorrectas!'
						})
						return;
					}

					calificaciones = JSON.stringify(calificaciones);
					console.log(calificaciones);
					$.post(SERVERURL + '/calificaciones/guardarNotas', { notas : calificaciones }, function(res){
						if(res == 'ok'){
							alerta.fire({
								title: '¡Calificaciones guardadas!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							alerta.fire({
								title: '¡No se pudo borrar las calificaciones!'
							})
						}
					});

				});
			}
		});

	});


	//Generar boleta por estudiante
	$('.boletaPorEstudiante').click(function(){
		$('#boletaGenerada').html('');

		alerta.fire({
			title: 'Buscar estudiante',
			html: `
				<input id="buscarEstudianteAjax" class="form-control" type="text" placeholder="Cédula o nombre..." autocomplete="off" />
				<div class="listaEstudiantesAjax"></div>
			`
		})
		var consultaAjax = [];
		$('#buscarEstudianteAjax').on('keyup', function(){

			var dato = $(this).val().trim();

			consultaAjax.push($.post(SERVERURL + '/estudiantes/buscarAjax',{dato:dato}, function(res){
				consultaAjax.forEach((c)=>{
					c.abort();
				});
				$('.listaEstudiantesAjax').html('');
				res = JSON.parse(res);
				if(res.length < 1 || dato == ''){
					$('.listaEstudiantesAjax').html('');
				}else{
					res.forEach((r)=>{

						$('.listaEstudiantesAjax').append(`<span class="estudiante-item d-block" cedula="${r.cedula}" nombreEstudiante="${r.nombres} ${r.apellidos}" nombreGrado="${r.nombre}" id_grado="${r.id_grado}" numero="${r.seccion_numero}">${r.nombres} ${r.apellidos} (${r.cedula})</span>`);

					});
				}

				$('.estudiante-item').click(function(){
					var cedula = $(this).attr('cedula');
					var nombreEstudiante = $(this).attr('nombreEstudiante');
					var nombreGrado = $(this).attr('nombreGrado');
					var numero = $(this).attr('numero');
					var id_grado = $(this).attr('id_grado');
					var periodo = '';
					alerta.fire({
						title: 'Seleccionar periodo',
						html: `
							<div id="contenido">
								<div class="spinner-border text-primary"></div>
							</div>
						`
					});

					$.post(SERVERURL + '/calificaciones/getPeriodosDeCedulaYGrado', {cedula:cedula, id_grado : id_grado}, function(res){

						res = JSON.parse(res);
						if(res.length < 1){
							alerta.fire({
								title: 'No hay notas para consultar.',
							});
							return;
						}else{

							var tabla = '<table class="table table-hover table-stripped"><th>Periodo</th>';
							if(res.length < 1){
								tabla += '<tr class="text-center"><td colspan="4">No hay estudiantes registrados.</td></tr>';
							}else{
								res.forEach((r)=>{
									tabla += `
										<tr class="link estudiante-item-periodo" periodo="${r.periodo}">
											<td>${r.periodo} - ${parseInt(r.periodo)+1}</td>
										</tr>
									`;
								});
							}
							tabla += '</table>';
							$('#contenido').html(tabla);

							$('.estudiante-item-periodo').click(function(){
								periodo = $(this).attr('periodo');

								$.post(SERVERURL + '/calificaciones/getNotasPorCedulaYGradoYPeriodo',{cedula:cedula, id_grado : id_grado, periodo : periodo}, function(res){
									res = JSON.parse(res);

									var notaFinal = 0;
									var tabla = `<h1 class="text-center">Boleta de Calificaciones</h1>
												 <h3 class="text-center">${nombreGrado} Sección ${numero}</h3>
											   	 <h3 class="text-center">${nombreEstudiante}</h3>
											   	 <h4 class="text-center mb-4">${periodo} - ${parseInt(periodo)+1}</h4>
									`;
			 						tabla += `<table id="tablaCalificaciones" class="table text-center mx-auto border">`;
									tabla += `
											<th>Materia</th>
											<th>Lapso 1</th>
											<th>Lapso 2</th>
											<th>Lapso 3</th>
											<th>Recuperación 1</th>
											<th>Recuperación 2</th>
											<th>Nota Final</th>
									`;
									res.forEach((r)=>{

										tabla += `
											<tr>
												<td>${r.nombre}</td>
												<td>${r.lapso1}</td>
												<td>${r.lapso2}</td>
												<td>${r.lapso3}</td>
										`;

										if( ( parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3) )/3 >= 10 || parseInt(r.lapso1) == 0 ||  parseInt(r.lapso2) == 0 || parseInt(r.lapso3) == 0 ){
											r.recu1 = 'No aplica';
											r.recu2 = 'No aplica';
											notaFinal = (parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3))/3;
											notaFinal = notaFinal.toFixed(2);
											notaFinal = Math.ceil(notaFinal);
										}else if(parseInt(r.recu1) >= 10){
											r.recu2 = 'No aplica';
											if(parseInt(r.recu1) == 0){
												notaFinal = (parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3))/3;
												notaFinal = notaFinal.toFixed(2);
												notaFinal = Math.ceil(notaFinal);
											}else{
												notaFinal = Math.ceil(parseInt(r.recu1));
											}
										}else{
											if(parseInt(r.recu2) == 0){
												notaFinal = (parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3))/3;
												notaFinal = notaFinal.toFixed(2);
												notaFinal = Math.ceil(notaFinal);
											}else{
												notaFinal = Math.ceil(parseInt(r.recu2));
											}
										}

										tabla += `
												<td>${r.recu1}</td>
												<td>${r.recu2}</td>
												<td>${notaFinal}</td>
											</tr>
										`;

									});

									//Consultar materias de arrastre
									if(id_grado > 1){


										$.post(SERVERURL + '/calificaciones/getNotasPorCedulaYGradoYPeriodo',{cedula:cedula, id_grado : id_grado-1, periodo : periodo}, function(res){

											res = JSON.parse(res);

											res.forEach((r)=>{

											tabla += `
												<tr>
													<td>${r.nombre} (Arrastre)</td>
													<td>${r.lapso1}</td>
													<td>${r.lapso2}</td>
													<td>${r.lapso3}</td>
											`;

											if( ( parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3) )/3 >= 10 || parseInt(r.lapso1) == 0 ||  parseInt(r.lapso2) == 0 || parseInt(r.lapso3) == 0 ){
												r.recu1 = 'No aplica';
												r.recu2 = 'No aplica';
												notaFinal = (parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3))/3;
												notaFinal = notaFinal.toFixed(2);
												notaFinal = Math.ceil(notaFinal);
											}else if(parseInt(r.recu1) >= 10){
												r.recu2 = 'No aplica';
												if(parseInt(r.recu1) == 0){
													notaFinal = (parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3))/3;
													notaFinal = notaFinal.toFixed(2);
													notaFinal = Math.ceil(notaFinal);
												}else{
													notaFinal = Math.ceil(parseInt(r.recu1));
												}
											}else{
												if(parseInt(r.recu2) == 0){
													notaFinal = (parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3))/3;
													notaFinal = notaFinal.toFixed(2);
													notaFinal = Math.ceil(notaFinal);
												}else{
													notaFinal = Math.ceil(parseInt(r.recu2));
												}
											}

											tabla += `
													<td>${r.recu1}</td>
													<td>${r.recu2}</td>
													<td>${notaFinal}</td>
												</tr>
											`;

										});

											tabla += `</table>`;
											$('#boletaGenerada').html(tabla);
											Swal.close();
											window.print();

										});

									}else{
										tabla += `</table>`;
										$('#boletaGenerada').html(tabla);
										Swal.close();
										window.print();
									}


									

								});


							});

							



						}

					});

					

				});

			}));

		});

	});


	//Boleta por seccion
	$('.boletaPorSeccion').click(function(){
		$('#boletaGenerada').html('');

		alerta.fire({
			title: 'Seleccionar grado',
			html: `
				<div id="contenido">
					<div class="spinner-border text-primary"></div>
				</div>
			`,
			confirmButtonText: 'Cerrar'
		});

		$.post(SERVERURL + '/calificaciones/getPeriodos', function(res){

			res = JSON.parse(res);
			var tabla = '<table class="table table-hover table-stripped"><th>Periodo</th>';
			if(res.length < 1){
				tabla += '<tr class="text-center"><td colspan="4">No hay estudiantes registrados.</td></tr>';
			}else{
				res.forEach((r)=>{
					tabla += `
						<tr class="link boletaPorSeccionPeriodo" periodo="${r.periodo}">
							<td>${r.periodo} - ${parseInt(r.periodo)+1}</td>
						</tr>
					`;
				});
			}
			tabla += '</table>';
			$('#contenido').html(tabla);


			$('.boletaPorSeccionPeriodo').click(function(){

				alerta.fire({
					title: 'Seleccionar grado',
					html: `
						<div id="contenido">
							<div class="spinner-border text-primary"></div>
						</div>
					`,
					confirmButtonText: 'Cerrar'
				});

				var periodo = $(this).attr('periodo');

				$.post(SERVERURL + '/calificaciones/getGradosPorPeriodo', {periodo : periodo}, function(res){

					res = JSON.parse(res);
					var tabla = '<table class="table table-hover table-stripped"><th>Grado</th>';
					if(res.length < 1){
						tabla += '<tr class="text-center"><td colspan="4">No hay estudiantes registrados.</td></tr>';
					}else{
						res.forEach((r)=>{
							tabla += `
								<tr class="link boletaPorPeriodoYGrado" id_grado="${r.id}">
									<td>${r.nombre}</td>
								</tr>
							`;
						});
					}
					tabla += '</table>';
					$('#contenido').html(tabla);

					$('.boletaPorPeriodoYGrado').click(function(){

						alerta.fire({
							title: 'Seleccionar grado',
							html: `
								<div id="contenido">
									<div class="spinner-border text-primary"></div>
								</div>
							`,
							confirmButtonText: 'Cerrar'
						});

						var id_grado = $(this).attr('id_grado');

						$.post(SERVERURL + '/calificaciones/getCedulasPorGradoYPeriodo', {id_grado : id_grado, periodo : periodo}, function(res2){

							res2 = JSON.parse(res2);

							res2.forEach((r2)=>{

								$.post(SERVERURL + '/calificaciones/getNotasPorCedulaYGradoYPeriodo',{cedula:r2.ci_estudiante, id_grado : id_grado, periodo : periodo}, function(res){
									res = JSON.parse(res);

									var notaFinal = 0;
									var tabla = `<h1 class="text-center">Boleta de Calificaciones</h1>
												 <h3 class="text-center">${r2.nombregrado} Sección ${r2.datos.seccion_numero}</h3>
											   	 <h3 class="text-center">${r2.datos.nombres} ${r2.datos.apellidos}</h3>
											   	 <h4 class="text-center mb-4">${periodo} - ${parseInt(periodo)+1}</h4>
									`;
			 						tabla += `<table id="tablaCalificaciones" class="table text-center mx-auto border">`;
									tabla += `
											<th>Materia</th>
											<th>Lapso 1</th>
											<th>Lapso 2</th>
											<th>Lapso 3</th>
											<th>Recuperación 1</th>
											<th>Recuperación 2</th>
											<th>Nota Final</th>
									`;
									res.forEach((r)=>{

										tabla += `
											<tr>
												<td>${r.nombre}</td>
												<td>${r.lapso1}</td>
												<td>${r.lapso2}</td>
												<td>${r.lapso3}</td>
										`;

										if( ( parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3) )/3 >= 10 || parseInt(r.lapso1) == 0 ||  parseInt(r.lapso2) == 0 || parseInt(r.lapso3) == 0 ){
											r.recu1 = 'No aplica';
											r.recu2 = 'No aplica';
											notaFinal = (parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3))/3;
											notaFinal = notaFinal.toFixed(2);
											notaFinal = Math.ceil(notaFinal);
										}else if(parseInt(r.recu1) >= 10){
											r.recu2 = 'No aplica';
											if(parseInt(r.recu1) == 0){
												notaFinal = (parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3))/3;
												notaFinal = notaFinal.toFixed(2);
												notaFinal = Math.ceil(notaFinal);
											}else{
												notaFinal = Math.ceil(parseInt(r.recu1));
											}
										}else{
											if(parseInt(r.recu2) == 0){
												notaFinal = (parseInt(r.lapso1) + parseInt(r.lapso2) + parseInt(r.lapso3))/3;
												notaFinal = notaFinal.toFixed(2);
												notaFinal = Math.ceil(notaFinal);
											}else{
												notaFinal = Math.ceil(parseInt(r.recu2));
											}
										}

										tabla += `
												<td>${r.recu1}</td>
												<td>${r.recu2}</td>
												<td>${notaFinal}</td>
											</tr>
										`;

									});

									tabla += `</table>`;
									var html = $('#boletaGenerada').html();
									$('#boletaGenerada').html(html + '<div class="page-break"></div>' + tabla);
								
								});

							});


							setTimeout(function(){
								Swal.close();
								window.print();
							},1000);


						});

					});

				});

			});


		});

	});


});