<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-3">
		<h2 class="text-center">Lista de profesores</h2>
		<table class="table table-hover table-stripped text-center">
			<th>Cédula</th>
			<th>Nombres</th>
			<th>Apellidos</th>
			<th>Teléfono</th>
			<th>Correo</th>
			<th>Materias</th>
			<th>Acciones</th>
			<?php if(count($profesores) < 1){ ?>
				<tr>
					<td colspan="5">No hay profesores registrados</td>
				</tr>
			<?php }else{ ?>
				<?php foreach($profesores as $profesor){ ?>
					<tr>
						<td class="cedula"><?php echo $profesor->cedula ?></td>
						<td class="nombres"><?php echo $profesor->nombres ?></td>
						<td class="apellidos"><?php echo $profesor->apellidos ?></td>
						<td class="telefono"><?php echo $profesor->telefono ?></td>
						<td class="correo"><?php echo $profesor->correo ?></td>
						<td class="materias">
							<span class="verMateriasProfesor link" cedula="<?php echo $profesor->cedula ?>" nombres="<?php echo $profesor->nombres ?>" apellidos="<?php echo $profesor->apellidos ?>">
								Ver (<?php echo $profesor->materias ?>)
							</span>
						</td>
						<td>
							<img class="editar editarProfesor" src="../views/img/editar.svg" height="25" width="25">
							<img class="borrar borrarProfesor ml-2" src="../views/img/borrar.svg" height="25" width="25">
						</td>
					</tr>
				<?php }?>
			<?php } ?>
		</table>

		<button class="btn btn-success d-block mx-auto mt-5 agregarProfesor" type="button">Agregar</button>
		<a class="d-block mx-auto w-25" href="<?php echo SERVERURL ?>/menu/">
			<button class="btn btn-info d-block mx-auto mt-5 w-100" type="button">Menú principal</button>
		</a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>