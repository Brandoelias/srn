<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-3">
		<h2 class="text-center">Lista de estudiantes</h2>
		<table class="table table-hover table-stripped text-center">
			<th>Cédula</th>
			<th>Nombres</th>
			<th>Apellidos</th>
			<th>Fecha de nacimiento</th>
			<th>Grado</th>
			<th>Acciones</th>
			<?php if(count($estudiantes) < 1){ ?>
				<tr>
					<td colspan="5">No hay estudiantes registrados</td>
				</tr>
			<?php }else{ ?>
				<?php foreach($estudiantes as $estudiante){ ?>
					<tr>
						<td class="cedula"><?php echo $estudiante->cedula ?></td>
						<td class="nombres"><?php echo $estudiante->nombres ?></td>
						<td class="apellidos"><?php echo $estudiante->apellidos ?></td>
						<td class="fecha_nacimiento"><?php echo $estudiante->fecha_nacimiento ?></td>
						<td class="grado"><span class="link verGradoDeEstudiante" id_grado="<?php echo $estudiante->id_grado ?>" nombre="<?php echo $estudiante->nombres ?>" cedula="<?php echo $estudiante->cedula ?>" numero="<?php echo $estudiante->seccion_numero ?>"><?php $estudiante->id_grado == '' ? print('No inscrito') : print($estudiante->nombre) ?></span></td>
						<td>
							<img class="editar editarEstudiante" src="../views/img/editar.svg" height="25" width="25">
							<img class="borrar borrarEstudiante ml-2" src="../views/img/borrar.svg" height="25" width="25">
						</td>
					</tr>
				<?php }?>
			<?php } ?>
		</table>

		<button class="btn btn-success d-block mx-auto mt-5 agregarEstudiante" type="button">Agregar</button>
		<a class="d-block mx-auto w-25" href="<?php echo SERVERURL ?>/menu/">
			<button class="btn btn-info d-block mx-auto mt-5 w-100" type="button">Menú principal</button>
		</a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>