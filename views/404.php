<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="pt-5">
		<div class="alert alert-danger d-block mx-auto w-25 text-center font-weight-bold">
			¡Página no encontrada!
		</div>
		<a class="w-25 d-block mx-auto" href="<?php echo SERVERURL ?>/">
			<button class="btn btn-primary d-block mx-auto w-100" type="button">Inicio</button>
		</a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>