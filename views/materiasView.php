<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-3">
		<h2 class="text-center">Lista de materias</h2>
		<table class="table table-hover table-stripped text-center">
			<th>Nombre</th>
			<th>Grado</th>
			<th>Acciones</th>
			<?php if(count($materias) < 1){ ?>
				<tr>
					<td colspan="5">No hay materias registradas</td>
				</tr>
			<?php }else{ ?>
				<?php foreach($materias as $materia){ ?>
					<tr>
						<input class="idMateria" type="hidden" value="<?php echo $materia->id ?>" />
						<td class="nombre"><?php echo $materia->nombre ?></td>
						<td>
							<?php
								if(count($materia->grado) > 0){
									if(count($materia->grado) == 1){
										echo "<span class='link verGrado'>";
										print_r($materia->grado[0]->nombre);
										echo "</span>";
									}else{
										$count = count($materia->grado);
										echo "<span class='link verGrado'>Ver ($count)</span>";
									}
								}else{
									echo '<span class="link verGrado">Ninguno</span>';
								}
							?>	
							</td>
						<td>
							<img class="editar editarMateria" src="../views/img/editar.svg" height="25" width="25">
							<img class="borrar borrarMateria ml-2" src="../views/img/borrar.svg" height="25" width="25">
						</td>
					</tr>
				<?php }?>
			<?php } ?>
		</table>

		<button class="btn btn-success d-block mx-auto mt-5 agregarMateria" type="button">Agregar</button>
		<a class="d-block mx-auto w-25" href="<?php echo SERVERURL ?>/menu/">
			<button class="btn btn-info d-block mx-auto mt-5 w-100" type="button">Menú principal</button>
		</a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>