<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-3">
		<h2 class="text-center">Gestión de calificaciones</h2>
		<h4 class="text-center"><?php echo $estudiante->nombres . ' ' . $estudiante->apellidos ?></h4>

		<table id="tablaCalificaciones" class="table table-hover table-stripped text-center">
			<th>Materia</th>
			<th>Lapso 1</th>
			<th>Lapso 2</th>
			<th>Lapso 3</th>
			<th>Recuperación 1</th>
			<th>Recuperación 2</th>
			<th>Nota Final</th>
			<?php foreach($materias as $materia){ ?>
				<?php 
					if(ceil(($materia->lapso1 + $materia->lapso2 + $materia->lapso3)/3) >= 10 || $materia->lapso1 == 0 && $materia->lapso2 == 0 || $materia->lapso3 == 0){
						$dnone = 'd-none';
						$dspan1 = '';
					}else{
						$dnone = '';
						$dspan1 = 'd-none';
					}
					if(ceil($materia->recu1) < 10 && $materia->recu1 != 0){
						$dnone2 = '';
						$dspan2 = 'd-none';
					}else{
						$dnone2 = 'd-none';
						$dspan2 = '';
					}
				?>
				<tr class="rowTabla" id_materia="<?php echo $materia->id ?>" id_grado="<?php echo $materia->id_grado ?>" cedula="<?php echo $estudiante->cedula ?>">
					<td>
						<?php echo $materia->nombre ?>
					</td>
					<td>
						<input id="lapso1" class="form-control w-50 d-block mx-auto lapso" type="number" value="<?php echo $materia->lapso1 ?>" max="20" min="0">
					</td>
					<td>
						<input id="lapso2" class="form-control w-50 d-block mx-auto lapso" type="number" value="<?php echo $materia->lapso2 ?>" max="20" min="0">
					</td>
					<td>
						<input id="lapso3" class="form-control w-50 d-block mx-auto lapso" type="number" value="<?php echo $materia->lapso3 ?>" max="20" min="0">
					</td>
					<td>
						<input id="recu1" class="form-control w-50 mx-auto <?php echo $dnone ?> recu" type="number" value="<?php echo $materia->recu1 ?>" max="20" min="0">
						<span id="recu1span" class="<?php echo $dspan1 ?>">No aplica</span>
					</td>
					<td>
						<input id="recu2" class="form-control w-50 mx-auto <?php echo $dnone2 ?> recu" type="number" value="<?php echo $materia->recu2 ?>" max="20" min="0">
						<span id="recu2span" class="<?php echo $dspan2 ?>">No aplica</span>
					</td>
					<td id="notaFinal">
						<?php
							if(ceil(($materia->lapso1 + $materia->lapso2 + $materia->lapso3)/3) >= 10 || $materia->lapso1 == 0 && $materia->lapso2 == 0 || $materia->lapso3 == 0){
								printf(ceil(($materia->lapso1 + $materia->lapso2 + $materia->lapso3)/3));
							}elseif($materia->recu1 != 0 && $materia->recu1 < 10){
								printf(ceil($materia->recu2));
							}else{
								printf(ceil($materia->recu1));
							}
						?>
					</td>
				</tr>
			<?php } ?>
			<?php foreach($materiasArrastre as $materia){ ?>
				<?php 
					if(ceil(($materia->lapso1 + $materia->lapso2 + $materia->lapso3)/3) >= 10 || $materia->lapso1 == 0 && $materia->lapso2 == 0 || $materia->lapso3 == 0){
						$dnone = 'd-none';
						$dspan1 = '';
					}else{
						$dnone = '';
						$dspan1 = 'd-none';
					}
					if(ceil($materia->recu1) < 10 && $materia->recu1 != 0){
						$dnone2 = '';
						$dspan2 = 'd-none';
					}else{
						$dnone2 = 'd-none';
						$dspan2 = '';
					}
				?>
				<tr class="rowTabla" id_materia="<?php echo $materia->id ?>" id_grado="<?php echo $materia->id_grado ?>" cedula="<?php echo $estudiante->cedula ?>">
					<td>
						<?php echo $materia->nombre ?> (Arrastre)
					</td>
					<td>
						<input id="lapso1" class="form-control w-50 d-block mx-auto lapso" type="number" value="<?php echo $materia->lapso1 ?>" max="20" min="0">
					</td>
					<td>
						<input id="lapso2" class="form-control w-50 d-block mx-auto lapso" type="number" value="<?php echo $materia->lapso2 ?>" max="20" min="0">
					</td>
					<td>
						<input id="lapso3" class="form-control w-50 d-block mx-auto lapso" type="number" value="<?php echo $materia->lapso3 ?>" max="20" min="0">
					</td>
					<td>
						<input id="recu1" class="form-control w-50 mx-auto <?php echo $dnone ?> recu" type="number" value="<?php echo $materia->recu1 ?>" max="20" min="0">
						<span id="recu1span" class="<?php echo $dspan1 ?>">No aplica</span>
					</td>
					<td>
						<input id="recu2" class="form-control w-50 mx-auto <?php echo $dnone2 ?> recu" type="number" value="<?php echo $materia->recu2 ?>" max="20" min="0">
						<span id="recu2span" class="<?php echo $dspan2 ?>">No aplica</span>
					</td>
					<td id="notaFinal">
						<?php
							if(ceil(($materia->lapso1 + $materia->lapso2 + $materia->lapso3)/3) >= 10 || $materia->lapso1 == 0 && $materia->lapso2 == 0 || $materia->lapso3 == 0){
								printf(ceil(($materia->lapso1 + $materia->lapso2 + $materia->lapso3)/3));
							}elseif($materia->recu1 != 0 && $materia->recu1 < 10){
								printf(ceil($materia->recu2));
							}else{
								printf(ceil($materia->recu1));
							}
						?>
					</td>
				</tr>
			<?php } ?>
		</table>

		<button class="btn btn-success d-block mx-auto guardarNotasEstudiante" type="button">Guardar</button>
		
		<a class="d-block mx-auto w-25" href="<?php echo SERVERURL ?>/menu/">
			<button class="btn btn-info d-block mx-auto mt-5 w-100" type="button">Menú principal</button>
		</a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>
<script>
	
	$(document).ready(function(){

		$('.rowTabla').find('input').on('keyup change', function(){
			var nota = 0;
			var lapsoPasado = true;
			$(this).parent().parent().find('.lapso').each((i)=>{
				var input = $(this).parent().parent().find('.lapso').get(i);
				if(input.value < 0 || input.value > 20){
					$(input).css('border', '1px solid red');
				}else{
					$(input).css('border', '');
				}
				if(input.value == 0){
					lapsoPasado = false;
				}
				nota += parseInt(input.value);
			})
			nota = nota/3;
			nota = nota.toFixed(2);
			nota = Math.ceil(nota);
			if(nota < 10 && lapsoPasado){
				$(this).parent().parent().find('#recu1').removeClass('d-none');
				$(this).parent().parent().find('#recu1span').addClass('d-none');
				var recu = $(this).parent().parent().find('#recu1').get(0);
				nota = parseInt(recu.value);
				nota = nota.toFixed(2);
				nota = Math.ceil(nota);
				if(recu.value < 10 && recu.value != 0){
					$(this).parent().parent().find('#recu2').removeClass('d-none');
					$(this).parent().parent().find('#recu2span').addClass('d-none');
					var recu2 = $(this).parent().parent().find('#recu2').get(0);
					nota = parseInt(recu2.value);
					nota = nota.toFixed(2);
					nota = Math.ceil(nota);
				}else{
					$(this).parent().parent().find('#recu2').addClass('d-none');
					$(this).parent().parent().find('#recu2').val(0);
					$(this).parent().parent().find('#recu2span').removeClass('d-none');
				}
			}else{
				$(this).parent().parent().find('#recu1').addClass('d-none');
				$(this).parent().parent().find('#recu1').val(0);
				$(this).parent().parent().find('#recu1span').removeClass('d-none');
				$(this).parent().parent().find('#recu2').addClass('d-none');
				$(this).parent().parent().find('#recu2').val(0);
				$(this).parent().parent().find('#recu2span').removeClass('d-none');
			}
			$(this).parent().parent().find('#notaFinal').text(nota);
		})

	});

</script>