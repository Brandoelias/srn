<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-3 text-center menu-items">
		<div class="jumbotron p-5 d-block mx-auto w-25 text-center menu-item" link="profesores">
			Profesores
		</div>
		<div class="jumbotron p-5 d-block mx-auto w-25 text-center menu-item" link="estudiantes">
			Estudiantes
		</div>
		<div class="jumbotron p-5 d-block mx-auto w-25 text-center menu-item" link="materias">
			Materias
		</div>
		<div class="jumbotron p-5 d-block mx-auto w-25 text-center menu-item" link="grados">
			Grados
		</div>
		<div class="jumbotron p-5 d-block mx-auto w-25 text-center menu-item" link="calificaciones">
			Calificaciones
		</div>
		<div class="jumbotron p-5 d-block mx-auto w-25 text-center menu-item" link="boletas">
			Boletas
		</div>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>