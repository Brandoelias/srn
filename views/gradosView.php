<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-3">
		<h2 class="text-center">Lista de grados</h2>
		<table class="table table-hover table-stripped text-center mx-auto" style="border: 2px solid black">
			<th>Año</th>
			<th>Profesores</th>
			<th>Estudiantes</th>
			<th>Sección/es</th>
			<?php if(count($grados) < 1){ ?>
				<tr>
					<td colspan="5">No hay grados registradas</td>
				</tr>
			<?php }else{ ?>
				<?php foreach($grados as $grado){ ?>
				
					<tr>
						<input class="idgrado" type="hidden" value="<?php echo $grado->id ?>" />
						<td class="nombre">
							<span><?php echo $grado->nombre ?></span>
						</td>
						<td class="profesores">
							<span><?php echo $grado->profesores ?></span>
						</td>
						<td class="estudiantes">
							<span><?php echo $grado->estudiantes ?></span>
						</td>
						<td>
							<span class="link verSecciones">
								Ver (<?php echo $grado->secciones ?>)	
							</span>
						</td>
					</tr>
				
				<?php }?>
			<?php } ?>
		</table>

		<a class="d-block mx-auto w-25" href="<?php echo SERVERURL ?>/menu/">
			<button class="btn btn-info d-block mx-auto mt-5 w-100" type="button">Menú principal</button>
		</a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>