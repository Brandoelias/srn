<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-3">
		<h2 class="text-center">Gestión de calificaciones</h2>

		<table class="table table-hover table-stripped text-center">
			<th>Grado</th>
			<?php foreach($grados as $grado){ ?>

				<tr>
					<td>
						<a class="link" href="<?php echo SERVERURL ?>/calificaciones/seccion/<?php echo $grado->id ?>/"><?php echo $grado->nombre ?></a>
					</td>
				</tr>

			<?php } ?>
		</table>
		
		<a class="d-block mx-auto w-25" href="<?php echo SERVERURL ?>/menu/">
			<button class="btn btn-info d-block mx-auto mt-5 w-100" type="button">Menú principal</button>
		</a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>