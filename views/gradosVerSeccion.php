<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-3">
		<h2 class="text-center"><?php echo $grado->nombre ?><br>Sección <?php echo $grado->numero ?></h2>
		
		<div class="menu-items mt-5 mb-5">
			<div class="jumbotron p-5 d-block mx-auto w-25 text-center menu-item" link="grados/estudiantes/<?php echo $idGrado . '/' . $idSeccion ?>">
				Estudiantes
			</div>
			<div class="jumbotron p-5 d-block mx-auto w-25 text-center menu-item" link="grados/estudiantes/<?php echo $idGrado . '/' . $idSeccion ?>">
				Profesores
			</div>
		</div>

		<a class="d-block mx-auto w-25" href="<?php echo SERVERURL ?>/menu/">
			<button class="btn btn-info d-block mx-auto mt-5 w-100" type="button">Menú principal</button>
		</a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>