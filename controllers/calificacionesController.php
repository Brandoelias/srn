<?php

	include_once('./models/calificacionesModel.php');

	class calificacionesController{

		private $model;

		public function __construct(){
			$this->model = new calificacionesModel();
		}

		public function index(){
			include_once('./models/gradosModel.php');
			$grados = new gradosModel();
			$grados = $grados->getTodos();
			include_once('./views/calificacionesIndexView.php');
		}

		public function seccion($url){
			include_once('./models/seccionModel.php');
			$secciones = new seccionModel();
			$secciones->setIdGrado($url[2]);
			$secciones = $secciones->getSeccionPorGrado();
			include_once('./views/calificacionesSeccionView.php');
		}

		public function gestionar($url){
			$id_grado = $url[2];
			$numero = $url[3];

			include_once('./models/estudianteModel.php');
			$estudiantes = new estudianteModel();
			$estudiantes->setIdGrado($id_grado);
			$estudiantes->setSeccionNumero($numero);
			$estudiantes = $estudiantes->getEstudiantesPorGradoYSeccion();

			include_once('./views/calificacionesGestionarView.php');
		}

		public function estudiante($url){
			$id_grado = $url[2];
			$numero = $url[3];
			$cedula = $url[4];

			include_once('./models/estudianteModel.php');
			$estudianteModel = new estudianteModel();
			$estudianteModel->setCedula($cedula);
			$materias = $estudianteModel->getNotasMaterias();
			$materiasArrastre = $estudianteModel->getNotasMateriasDeArrastre();
			$estudiante = $estudianteModel->getDatosPorCedula();

			include_once('./views/calificacionesEstudianteView.php');
		}

		public function guardarNotas(){
			$notas = json_decode($_POST['notas']);
			echo $this->model->guardarNotas($notas);
		}

		public function getNotasPorCedula(){
			$cedula = $_POST['cedula'];
			echo json_encode($this->model->getNotasPorCedula($cedula));
		}

		public function getNotasPorCedulaYGrado(){
			$cedula = $_POST['cedula'];
			$id_grado = $_POST['id_grado'];
			echo json_encode($this->model->getNotasPorCedulaYGrado($cedula, $id_grado));
		}
		
		public function getPeriodosDeCedulaYGrado(){
			$cedula = $_POST['cedula'];
			$id_grado = $_POST['id_grado'];
			echo json_encode($this->model->getPeriodosDeCedulaYGrado($cedula, $id_grado));
		}

		public function getNotasPorCedulaYGradoYPeriodo(){
			$cedula = $_POST['cedula'];
			$id_grado = $_POST['id_grado'];
			$periodo = $_POST['periodo'];
			$res = $this->model->getNotasPorCedulaYGradoYPeriodo($cedula, $id_grado, $periodo);
			echo json_encode($res);
		}

		public function getPeriodos(){
			echo json_encode($this->model->getPeriodos());
		}

		public function getGradosPorPeriodo(){
			$periodo = $_POST['periodo'];
			echo json_encode($this->model->getGradosPorPeriodo($periodo));
		}

		public function getCedulasPorGradoYPeriodo(){
			$id_grado = $_POST['id_grado'];
			$periodo = $_POST['periodo'];
			$res = $this->model->getCedulasPorGradoYPeriodo($periodo, $id_grado);
			foreach ($res as $r) {
				$r->datos = $this->model->consultarEstudiantePorCedula($r->ci_estudiante);
			}
			echo json_encode($res);
		}


	}
?>