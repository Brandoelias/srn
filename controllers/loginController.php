<?php

	include_once('./models/usuarioModel.php');

	class loginController{

		private $model;

		public function __construct(){
			$this->model = new loginModel();
		}

		public function index(){
			if(isset($_SESSION['id'])){
				header('location: ' . SERVERURL . '/menu/');
				die();
			}
			include_once('./views/loginView.php');
		}

		public function check(){
			$usuario = $_POST['usuario'];
			$pass = $_POST['pass'];
			$this->model->setNombre($usuario);
			$this->model->setClave($pass);
			$res = $this->model->check();
			if($res == 'no'){
				die('no');
			}
			$datos = $res->fetchAll(PDO::FETCH_OBJ);
			$clave = $datos[0]->clave;

			$verificar = password_verify($pass, $clave);

			if($verificar){
				$_SESSION['usuario'] = $usuario;
				$_SESSION['id'] = $datos[0]->id;
				echo 'ok';
			}else{
				echo 'no';
			}
		}



	}

?>