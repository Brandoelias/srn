<?php

	include_once('./models/seccionModel.php');

	class seccionController{

		private $model;

		public function __construct(){
			$this->model = new seccionModel();
		}

		public function getSeccionPorGrado(){
			$id_grado = $_POST['id'];
			$this->model->setIdGrado($id_grado);
			echo json_encode($this->model->getSeccionPorGrado());
		}

		public function agregarAGrado(){
			$id_grado = $_POST['idGrado'];
			$numero = $_POST['numero'];
			$this->model->setIdGrado($id_grado);
			$this->model->setNumero($numero);

			echo $this->model->agregarAGrado();
		}

		public function borrarSeccionDeGrado(){
			$id_grado = $_POST['idGrado'];
			$numero = $_POST['numero'];
			$this->model->setIdGrado($id_grado);
			$this->model->setNumero($numero);

			echo $this->model->borrarSeccionDeGrado();
		}
		
	}

?>