<?php

	include_once('./models/gradosModel.php');

	class gradosController{

		private $model;

		public function __construct(){
			$this->model = new gradosModel();
		}

		public function index(){
			$grados = $this->model->getTodos();
			foreach ($grados as $grado) {
				$grado->secciones = $this->model->getCantidadDeSeccionesPorGrado($grado->id);
				$grado->estudiantes = $this->model->getTotalDeEstudiantesPorGrado($grado->id);
				$grado->profesores = $this->model->getTotalDeProfesoresPorGrado($grado->id);
			}
			include_once('./views/gradosView.php');
		}

		public function ver($id){
			$idGrado = $id[2];
			$idSeccion = $id[3];
			$this->model->setId($idGrado);
			$grado = $this->model->getDatosPorId();
			include_once('./views/gradosVerSeccion.php');
		}

		public function getTodos(){
			echo json_encode($this->model->getTodos());
		}

	}

?>