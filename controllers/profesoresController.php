<?php

	include_once('./models/profesorModel.php');

	class profesoresController{

		private $cedula;
		private $nombres;
		private $apellidos;
		private $telefono;
		private $correo;
		private $model;

		public function __construct(){
			if(!isset($_SESSION['id'])){
				header('location: ' . SERVERURL . '/');
				die();
			}
			$this->model = new profesorModel();
		}

		public function setCedula($cedula){
			$this->cedula = $cedula;
		}
		public function setNombres($nombres){
			$this->nombre = $nombres;
		}
		public function setApellido($apellidos){
			$this->apellido = $apellidos;
		}
		public function setTelefono($telefono){
			$this->telefono = $telefono;
		}
		public function setCorreo($correo){
			$this->correo = $correo;
		}

		public function index(){
			$profesores = $this->model->getTodos();
			foreach ($profesores as $profesor) {
				$this->model->setCedula($profesor->cedula);
				$profesor->materias = $this->model->getCantidadDeMaterias();
			}
			include_once('./views/profesorView.php');
		}

		public function guardar(){
			$cedula = $_POST['cedula'];
			$nombres = $_POST['nombres'];
			$apellidos = $_POST['apellidos'];
			$telefono = $_POST['telefono'];
			$correo = $_POST['correo'];

			$this->model->setCedula($cedula);
			$this->model->setNombres($nombres);
			$this->model->setApellidos($apellidos);
			$this->model->setTelefono($telefono);
			$this->model->setCorreo($correo);

			echo $this->model->guardar();
		}

		public function editar(){
			$cedulaOriginal = $_POST['cedulaOriginal'];
			$cedulaModificar = $_POST['cedulaModificar'];
			$nombres = $_POST['nombres'];
			$apellidos = $_POST['apellidos'];
			$telefono = $_POST['telefono'];
			$correo = $_POST['correo'];

			$this->model->setCedula($cedulaOriginal);
			$this->model->setNombres($nombres);
			$this->model->setApellidos($apellidos);
			$this->model->setTelefono($telefono);
			$this->model->setCorreo($correo);

			echo $this->model->editar($cedulaModificar);
		}

		public function borrar(){
			$cedula = $_POST['cedula'];
			$this->model->setCedula($cedula);
			echo $this->model->borrar();
		}

		public function getMaterias(){
			$cedula = $_POST['cedula'];
			$this->model->setCedula($cedula);
			echo json_encode($this->model->getMaterias());
		}

		public function agregarMaterias(){
			$materias = json_decode($_POST['materias']);
			$cedula = $_POST['cedula'];
			$id_grado = $_POST['id_grado'];
			$seccion = $_POST['seccion'];
			$this->model->setCedula($cedula);
			echo $this->model->agregarMaterias($materias, $id_grado, $seccion);
		}

		public function getMateriasPorGradoNotIn(){
			$cedula = $_POST['cedula'];
			$id_grado = $_POST['id_grado'];
			$seccion = $_POST['seccion'];
			$this->model->setCedula($cedula);
			echo json_encode($this->model->getMateriasPorGradoNotIn($id_grado, $seccion));
		}

		public function borrarMateria(){
			$cedula = $_POST['cedula'];
			$id_grado = $_POST['id_grado'];
			$numero = $_POST['numero'];
			$id_materia = $_POST['id_materia'];
			$this->model->setCedula($cedula);
			echo$this->model->borrarMateria($id_grado, $id_materia, $numero);
		}

	}

?>