<?php

	include_once('./models/menuModel.php');

	class menuController{

		private $model;

		public function __construct(){
			$this->model = new menuModel();
		}

		public function index(){
			if(!isset($_SESSION['id'])){
				header('location: ' . SERVERURL . '/login/');
				die();
			}

			include_once('./views/menuView.php');
		}

	}

?>