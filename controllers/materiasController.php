<?php

	include_once('./models/materiasModel.php');

	class materiasController{

		private $model;

		public function __construct(){
			$this->model = new materiasModel();
		}

		public function index(){
			$materias = $this->model->getTodos();
			foreach ($materias as $materia) {
				$materia->grado = $this->model->getGradoPorId($materia->id);
			}
			include_once('./views/materiasView.php');
		}

		public function agregar(){
			$nombre = $_POST['nombre'];
			$this->model->setNombre($nombre);
			echo $this->model->agregar();
		}

		public function editar(){
			$id = $_POST['idMateria'];
			$nombre = $_POST['nombre'];
			$this->model->setId($id);
			$this->model->setNombre($nombre);
			echo $this->model->editar();
		}

		public function borrar(){
			$id = $_POST['idMateria'];
			$this->model->setId($id);
			echo $this->model->borrar();
		}

		public function getGradosNotIn(){
			$id = $_POST['idMateria'];
			$this->model->setId($id);
			echo json_encode($this->model->getGradosNotIn());
		}

		public function agregarGrado(){
			$id = $_POST['idMateria'];
			$idGrado = $_POST['idGrado'];
			$this->model->setId($id);
			echo $this->model->agregarGrado($idGrado);
		}

		public function getGradosPorIdMateria(){
			$id = $_POST['idMateria'];
			$this->model->setId($id);
			echo json_encode($this->model->getGradosPorIdMateria($id));
		}

		public function borrarGradoDeMateria(){
			$id = $_POST['idMateria'];
			$idGrado = $_POST['idGrado'];
			$this->model->setId($id);
			echo $this->model->borrarGradoDeMateria($idGrado);
		}

		public function getMateriasPorGrado(){
			$id_grado = $_POST['id_grado'];
			echo json_encode($this->model->getMateriasPorGrado($id_grado));
		}

	}

?>