<?php

	include_once('./models/estudianteModel.php');

	class estudiantesController{

		private $model;

		public function __construct(){
			if(!isset($_SESSION['id'])){
				header('location: ' . SERVERURL . '/');
				die();
			}
			$this->model = new estudianteModel();
		}

		public function index(){
			$estudiantes = $this->model->getTodos();
			include_once('./views/estudianteView.php');
		}

		public function guardar(){
			$cedula = $_POST['cedula'];
			$nombres = $_POST['nombres'];
			$apellidos = $_POST['apellidos'];
			$fecha_nacimiento = $_POST['fecha_nacimiento'];

			$this->model->setCedula($cedula);
			$this->model->setNombres($nombres);
			$this->model->setApellidos($apellidos);
			$this->model->setFechaNacimiento($fecha_nacimiento);

			echo $this->model->guardar();
		}

		public function editar(){
			$cedulaOriginal = $_POST['cedulaOriginal'];
			$cedulaModificar = $_POST['cedulaModificar'];
			$nombres = $_POST['nombres'];
			$apellidos = $_POST['apellidos'];
			$fecha_nacimiento = $_POST['fecha_nacimiento'];

			$this->model->setCedula($cedulaOriginal);
			$this->model->setNombres($nombres);
			$this->model->setApellidos($apellidos);
			$this->model->setFechaNacimiento($fecha_nacimiento);

			echo $this->model->editar($cedulaModificar);
		}

		public function borrar(){
			$cedula = $_POST['cedula'];
			$this->model->setCedula($cedula);
			echo $this->model->borrar();
		}

		public function verGradoDeEstudiante(){
			$cedula = $_POST['cedula'];
			$this->model->setCedula($cedula);
			echo json_encode($this->model->verGradoDeEstudiante());
		}

		public function inscribirEstudianteAGrado(){
			$id_grado = $_POST['id_grado'];
			$seccion = $_POST['seccion'];
			$cedula = $_POST['cedula'];
			$this->model->setIdGrado($id_grado);
			$this->model->setCedula($cedula);
			$this->model->setSeccionNumero($seccion);
			echo $this->model->inscribirEstudianteAGrado();
		}

		public function borrarGradoDeEstudiante(){
			$cedula = $_POST['cedula'];
			$this->model->setCedula($cedula);
			echo $this->model->borrarGradoDeEstudiante();
		}

		public function agregarMaterias(){
			$materias = json_decode($_POST['materias']);
			$id_grado = $_POST['id_grado'];
			$materiasArrastre = json_decode($_POST['materiasArrastre']);
			$cedula = $_POST['cedula'];
			$periodo = $_POST['periodo'];
			$this->model->setCedula($cedula);
			$this->model->setIdGrado($id_grado);
			echo $this->model->agregarMaterias($materias, $materiasArrastre, $periodo);
		}

		public function getCantidadDeMaterias(){
			$cedula = $_POST['cedula'];
			$this->model->setCedula($cedula);

			$materias = $this->model->getCantidadDeMaterias();
			$arrastre = $this->model->getCantidadDeMateriasDeArrastre();

			echo '{"materias" : "' . $materias . '", "arrastre" : "' .  $arrastre. '"}';
		}

		public function getMaterias(){
			$cedula = $_POST['cedula'];
			$this->model->setCedula($cedula);
			echo json_encode($this->model->getMaterias());
		}

		public function getMateriasDeArrastre(){
			$cedula = $_POST['cedula'];
			$this->model->setCedula($cedula);
			echo json_encode($this->model->getMateriasDeArrastre());
		}

		public function borrarMateriaDeEstudiante(){
			$id_materia = $_POST['id_materia'];
			$id_grado = $_POST['id_grado'];
			$cedula = $_POST['cedula'];
			$this->model->setCedula($cedula);
			echo $this->model->borrarMateriaDeEstudiante($id_materia, $id_grado);
		}

		public function getMateriasNotIn(){
			$cedula = $_POST['cedula'];
			$id_grado = $_POST['id_grado'];
			$this->model->setCedula($cedula);
			echo json_encode($this->model->getMateriasNotIn($id_grado));
		}

		public function getMateriasDeArrastreNotIn(){
			$cedula = $_POST['cedula'];
			$id_grado = $_POST['id_grado'];
			$this->model->setCedula($cedula);
			echo json_encode($this->model->getMateriasDeArrastreNotIn($id_grado));
		}

		public function getEstudiantesPorGradoYSeccion(){
			$id_grado = $_POST['id_grado'];
			$numero = $_POST['numero'];
			$this->model->setIdGrado($id_grado);
			$this->model->setSeccionNumero($numero);
			echo json_encode($this->model->getEstudiantesPorGradoYSeccion($numero));
		}

		public function buscarAjax(){
			$dato = json_decode($_POST['dato']);
			echo json_encode($this->model->buscarAjax($dato));
		}

	}

?>