--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Debian 11.4-1)
-- Dumped by pg_dump version 11.4 (Debian 11.4-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: estudiante; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estudiante (
    cedula integer NOT NULL,
    nombres character varying(100),
    apellidos character varying(100),
    fecha_nacimiento date,
    id_grado integer,
    seccion_numero integer
);


ALTER TABLE public.estudiante OWNER TO postgres;

--
-- Name: grado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grado (
    id integer NOT NULL,
    nombre character varying(30)
);


ALTER TABLE public.grado OWNER TO postgres;

--
-- Name: grado_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grado_id_seq OWNER TO postgres;

--
-- Name: grado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grado_id_seq OWNED BY public.grado.id;


--
-- Name: grado_materia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grado_materia (
    id_grado integer NOT NULL,
    id_materia integer NOT NULL
);


ALTER TABLE public.grado_materia OWNER TO postgres;

--
-- Name: materia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.materia (
    id integer NOT NULL,
    nombre character varying(50)
);


ALTER TABLE public.materia OWNER TO postgres;

--
-- Name: materia_estudiante; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.materia_estudiante (
    id_materia integer NOT NULL,
    ci_estudiante integer NOT NULL,
    id_grado integer NOT NULL
);


ALTER TABLE public.materia_estudiante OWNER TO postgres;

--
-- Name: materia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.materia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.materia_id_seq OWNER TO postgres;

--
-- Name: materia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.materia_id_seq OWNED BY public.materia.id;


--
-- Name: materia_profesor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.materia_profesor (
    id_materia integer NOT NULL,
    ci_profesor integer NOT NULL,
    id_grado integer NOT NULL,
    seccion_numero integer
);


ALTER TABLE public.materia_profesor OWNER TO postgres;

--
-- Name: nota_materia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nota_materia (
    id_materia integer NOT NULL,
    ci_estudiante integer NOT NULL,
    id_grado integer NOT NULL,
    lapso1 real DEFAULT 0,
    lapso2 real DEFAULT 0,
    lapso3 real DEFAULT 0,
    recu1 real DEFAULT 0,
    recu2 real DEFAULT 0,
    periodo character varying(5) NOT NULL
);


ALTER TABLE public.nota_materia OWNER TO postgres;

--
-- Name: profesor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profesor (
    cedula integer NOT NULL,
    nombres character varying(100),
    apellidos character varying(100),
    telefono character varying(15),
    correo character varying(50)
);


ALTER TABLE public.profesor OWNER TO postgres;

--
-- Name: seccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seccion (
    id_grado integer NOT NULL,
    numero integer NOT NULL
);


ALTER TABLE public.seccion OWNER TO postgres;

--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id integer NOT NULL,
    nombre character varying(50),
    clave character varying(255)
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_seq OWNER TO postgres;

--
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuario.id;


--
-- Name: grado id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado ALTER COLUMN id SET DEFAULT nextval('public.grado_id_seq'::regclass);


--
-- Name: materia id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia ALTER COLUMN id SET DEFAULT nextval('public.materia_id_seq'::regclass);


--
-- Name: usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id SET DEFAULT nextval('public.usuario_id_seq'::regclass);


--
-- Data for Name: estudiante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estudiante (cedula, nombres, apellidos, fecha_nacimiento, id_grado, seccion_numero) FROM stdin;
26728989	Brando Elias	Marcano Ortiz	1997-04-23	1	1
34234234	dsfsfs	fsdfsdf	1995-01-18	2	2
3423423	sdfsdf	sdfsdf	1995-01-12	1	1
\.


--
-- Data for Name: grado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grado (id, nombre) FROM stdin;
1	Primer año
2	Segundo año
3	Tercer año
4	Cuarto año
5	Quinto año
6	Sexto año
0	No inscrito
\.


--
-- Data for Name: grado_materia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grado_materia (id_grado, id_materia) FROM stdin;
1	2
3	3
1	4
2	5
2	6
2	7
\.


--
-- Data for Name: materia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.materia (id, nombre) FROM stdin;
3	Castellano
4	Artistica
2	Matemáticas
5	Física
6	Química
7	Historia
\.


--
-- Data for Name: materia_estudiante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.materia_estudiante (id_materia, ci_estudiante, id_grado) FROM stdin;
2	26728989	1
3	26728989	1
4	26728989	1
2	34234234	2
5	34234234	2
6	34234234	2
7	34234234	2
3	34234234	1
2	3423423	1
3	3423423	1
4	3423423	1
\.


--
-- Data for Name: materia_profesor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.materia_profesor (id_materia, ci_profesor, id_grado, seccion_numero) FROM stdin;
4	26728989	1	1
3	26728989	3	1
2	26728989	2	1
5	26728989	2	1
7	26728989	2	1
6	25872399	2	1
\.


--
-- Data for Name: nota_materia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nota_materia (id_materia, ci_estudiante, id_grado, lapso1, lapso2, lapso3, recu1, recu2, periodo) FROM stdin;
2	26728989	1	0	0	0	0	0	2020
4	26728989	1	0	0	0	0	0	2020
2	34234234	2	0	0	0	0	0	2020
5	34234234	2	0	0	0	0	0	2020
6	34234234	2	0	0	0	0	0	2020
7	34234234	2	0	0	0	0	0	2020
2	3423423	1	0	0	0	0	0	2020
4	3423423	1	0	0	0	0	0	2020
\.


--
-- Data for Name: profesor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.profesor (cedula, nombres, apellidos, telefono, correo) FROM stdin;
25872399	asdasd	asdad	asdasd	asdasd
4554646	sdfsfs	sdfsdf	123123	sdfsdf
3423423	sdfsdfá	fgdfasdasdxd	13123123	dfgdfg
26728989	Brando Elias	Marcano Ortiz	04262111504	bmarcano@ubv.edu.ve
\.


--
-- Data for Name: seccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.seccion (id_grado, numero) FROM stdin;
3	1
5	1
6	1
2	1
2	2
1	1
\.


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id, nombre, clave) FROM stdin;
1	admin	$2y$12$NIJNm/ILUNzdXJWn97oFGu3fc8EW6mYaU/8RxQocQWm1uVpnjHH4q
\.


--
-- Name: grado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grado_id_seq', 6, true);


--
-- Name: materia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.materia_id_seq', 7, true);


--
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_seq', 1, true);


--
-- Name: estudiante estudiante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante
    ADD CONSTRAINT estudiante_pkey PRIMARY KEY (cedula);


--
-- Name: grado_materia grado_materia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado_materia
    ADD CONSTRAINT grado_materia_pkey PRIMARY KEY (id_grado, id_materia);


--
-- Name: grado grado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado
    ADD CONSTRAINT grado_pkey PRIMARY KEY (id);


--
-- Name: materia_estudiante materia_estudiante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia_estudiante
    ADD CONSTRAINT materia_estudiante_pkey PRIMARY KEY (id_materia, ci_estudiante, id_grado);


--
-- Name: materia materia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia
    ADD CONSTRAINT materia_pkey PRIMARY KEY (id);


--
-- Name: materia_profesor materia_profesor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia_profesor
    ADD CONSTRAINT materia_profesor_pkey PRIMARY KEY (id_materia, ci_profesor, id_grado);


--
-- Name: nota_materia nota_materia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nota_materia
    ADD CONSTRAINT nota_materia_pkey PRIMARY KEY (id_materia, ci_estudiante, id_grado, periodo);


--
-- Name: profesor profesor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profesor
    ADD CONSTRAINT profesor_pkey PRIMARY KEY (cedula);


--
-- Name: seccion seccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seccion
    ADD CONSTRAINT seccion_pkey PRIMARY KEY (id_grado, numero);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- Name: estudiante estudiante_id_grado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante
    ADD CONSTRAINT estudiante_id_grado_fkey FOREIGN KEY (id_grado) REFERENCES public.grado(id);


--
-- Name: grado_materia grado_materia_id_grado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado_materia
    ADD CONSTRAINT grado_materia_id_grado_fkey FOREIGN KEY (id_grado) REFERENCES public.grado(id);


--
-- Name: grado_materia grado_materia_id_materia_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado_materia
    ADD CONSTRAINT grado_materia_id_materia_fkey FOREIGN KEY (id_materia) REFERENCES public.materia(id);


--
-- Name: materia_estudiante materia_estudiante_ci_estudiante_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia_estudiante
    ADD CONSTRAINT materia_estudiante_ci_estudiante_fkey FOREIGN KEY (ci_estudiante) REFERENCES public.estudiante(cedula);


--
-- Name: materia_estudiante materia_estudiante_id_grado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia_estudiante
    ADD CONSTRAINT materia_estudiante_id_grado_fkey FOREIGN KEY (id_grado) REFERENCES public.grado(id);


--
-- Name: materia_estudiante materia_estudiante_id_materia_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia_estudiante
    ADD CONSTRAINT materia_estudiante_id_materia_fkey FOREIGN KEY (id_materia) REFERENCES public.materia(id);


--
-- Name: materia_profesor materia_profesor_ci_profesor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia_profesor
    ADD CONSTRAINT materia_profesor_ci_profesor_fkey FOREIGN KEY (ci_profesor) REFERENCES public.profesor(cedula);


--
-- Name: materia_profesor materia_profesor_id_grado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia_profesor
    ADD CONSTRAINT materia_profesor_id_grado_fkey FOREIGN KEY (id_grado) REFERENCES public.grado(id);


--
-- Name: materia_profesor materia_profesor_id_materia_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia_profesor
    ADD CONSTRAINT materia_profesor_id_materia_fkey FOREIGN KEY (id_materia) REFERENCES public.materia(id);


--
-- Name: nota_materia nota_materia_ci_estudiante_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nota_materia
    ADD CONSTRAINT nota_materia_ci_estudiante_fkey FOREIGN KEY (ci_estudiante) REFERENCES public.estudiante(cedula);


--
-- Name: nota_materia nota_materia_id_grado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nota_materia
    ADD CONSTRAINT nota_materia_id_grado_fkey FOREIGN KEY (id_grado) REFERENCES public.grado(id);


--
-- Name: nota_materia nota_materia_id_materia_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nota_materia
    ADD CONSTRAINT nota_materia_id_materia_fkey FOREIGN KEY (id_materia) REFERENCES public.materia(id);


--
-- Name: seccion seccion_id_grado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seccion
    ADD CONSTRAINT seccion_id_grado_fkey FOREIGN KEY (id_grado) REFERENCES public.grado(id);


--
-- PostgreSQL database dump complete
--

