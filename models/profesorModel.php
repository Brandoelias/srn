<?php


	class profesorModel{

		private $cedula;
		private $nombres;
		private $apellidos;
		private $telefono;
		private $correo;
		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function setCedula($cedula){
			$this->cedula = $cedula;
		}
		public function setNombres($nombres){
			$this->nombres = $nombres;
		}
		public function setApellidos($apellidos){
			$this->apellidos = $apellidos;
		}
		public function setTelefono($telefono){
			$this->telefono = $telefono;
		}
		public function setCorreo($correo){
			$this->correo = $correo;
		}

		public function getTodos(){
			try{
				$res = $this->db->prepare('SELECT * FROM profesor');
				$res->execute();
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function guardar(){
			try{
				$res = $this->db->prepare('INSERT INTO profesor (cedula, nombres, apellidos, telefono, correo) VALUES(?, ?, ?, ?, ?)');
				$res->execute([$this->cedula, $this->nombres, $this->apellidos, $this->telefono, $this->correo]);
			}catch(Exception $e){
				return 'no';
			}
			
			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function editar($cedula = null){
			try{
				$res = $this->db->prepare('UPDATE profesor SET cedula = ?, nombres = ?, apellidos = ?, telefono = ?, correo = ? WHERE cedula = ?');
				$res->execute([$cedula, $this->nombres, $this->apellidos, $this->telefono, $this->correo, $this->cedula]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function borrar(){
			try{
				$res = $this->db->prepare('DELETE FROM profesor WHERE cedula = ?');
				$res->execute([$this->cedula]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function getCantidadDeMaterias(){
			try{
				$res = $this->db->prepare('SELECT COUNT(id_materia) FROM materia_profesor WHERE ci_profesor = ?');
				$res->execute([$this->cedula]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res[0]->count;
		}

		public function getMaterias(){
			try{
				$res = $this->db->prepare('SELECT *,grado.nombre AS nombre_grado,materia.nombre AS nombre_materia,grado.id AS id_grado,materia.id AS id_materia FROM materia_profesor,materia,grado WHERE ci_profesor = ? AND materia.id = materia_profesor.id_materia AND grado.id = materia_profesor.id_grado');
				$res->execute([$this->cedula]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function agregarMaterias($materias, $id_grado, $seccion){
			foreach ($materias as $materia) {
				try{
					$res = $this->db->prepare('INSERT INTO materia_profesor(id_materia, ci_profesor, id_grado, seccion_numero) VALUES(?, ?, ?, ?)');
					$res->execute([$materia, $this->cedula, $id_grado, $seccion]);
				}catch(Exception $e){
					
				}
			}
			return 'ok';
		}

		public function getMateriasPorGradoNotIn($id_grado, $seccion){
			try{
				$res = $this->db->prepare('SELECT * FROM materia,grado_materia WHERE materia.id = grado_materia.id_materia AND grado_materia.id_grado = ? AND id_materia NOT IN(SELECT id_materia FROM materia_profesor WHERE id_grado = ? AND seccion_numero = ?)');
				$res->execute([$id_grado, $id_grado, $seccion]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function borrarMateria($id_grado, $id_materia, $numero){
			try{
				$res = $this->db->prepare('DELETE FROM materia_profesor WHERE id_materia = ? AND ci_profesor = ? AND id_grado = ? AND seccion_numero = ?');
				$res->execute([$id_materia, $this->cedula, $id_grado, $numero]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

	}


?>