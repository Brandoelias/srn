<?php

	class seccionModel{

		private $id_grado;
		private $numero;
		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function setIdGrado($id){
			$this->id_grado = $id;
		}
		public function setNumero($n){
			$this->numero = $n;
		}

		public function getSeccionPorGrado(){
			try{
				$res = $this->db->prepare('SELECT * FROM seccion WHERE id_grado = ?');
				$res->execute([$this->id_grado]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function agregarAGrado(){
			try{
				$res = $this->db->prepare('INSERT INTO seccion(id_grado, numero) VALUES(?, ?)');
				$res->execute([$this->id_grado, $this->numero]);
			}catch(Exception $e){
				return 'no';
			}
			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function borrarSeccionDeGrado(){
			try{
				$res = $this->db->prepare('DELETE FROM seccion WHERE id_grado = ? AND numero = ?');
				$res->execute([$this->id_grado, $this->numero]);
			}catch(Exception $e){
				return 'no';
			}
			if($res->rowCount() > 0){
				$this->db->query("UPDATE estudiante SET id_grado = 0, seccion_numero = 0 WHERE id_grado = $this->id_grado AND seccion_numero = $this->numero");
				return 'ok';
			}else{
				return 'no';
			}
		}

	}

?>