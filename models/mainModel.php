<?php

	class mainModel{


		public static function getFechaActualFormateada(){
			$d = Date('d');
			$m = intval(Date('m')) - 1;
			$a = Date('Y');
			$h = Date('h');
			$mi = Date('i');
			$s = Date('s');
			$me = Date('a');
			$meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
			return "$d de $meses[$m] del $a - $h:$mi:$s $me"; 
		}

		public static function conectar() {
			try {
				$host = HOST_DB;
				$port = PORT_DB;
				$dbname = NOMBRE_DB;
				$driver = DRIVER_DB;

	    		$conexion = new PDO("$driver:host=$host;port=$port;dbname=$dbname", USUARIO_DB, CLAVE_DB);
	    		$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}catch (Exception $e) {
	    		die("Ocurrió un error con la base de datos: " . $e->getMessage());
			}
			return $conexion;
		}


	}


?>
