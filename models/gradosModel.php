<?php

	class gradosModel{

		private $id;
		private $nombre;
		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function setId($id){
			$this->id = $id;
		}

		public function getTodos(){
			try{
				$res = $this->db->prepare('SELECT * FROM grado WHERE id != 0');
				$res->execute();
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function getCantidadDeSeccionesPorGrado($idGrado){
			try{
				$res = $this->db->prepare('SELECT COUNT(id_grado) FROM seccion WHERE id_grado = ?');
				$res->execute([$idGrado]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res[0]->count;
		}

		public function getDatosPorId(){
			try{
				$res = $this->db->prepare('SELECT * FROM grado,seccion WHERE id = ? AND grado.id = seccion.id_grado');
				$res->execute([$this->id]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res[0];
		}

		public function getTotalDeEstudiantesPorGrado($id_grado){
			try{
				$res = $this->db->prepare('SELECT COUNT(cedula) FROM estudiante WHERE id_grado = ?');
				$res->execute([$id_grado]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res[0]->count;
		}

		public function getTotalDeProfesoresPorGrado($id_grado){
			try{
				$res = $this->db->prepare('SELECT DISTINCT COUNT(ci_profesor) FROM materia_profesor WHERE id_grado = ?');
				$res->execute([$id_grado]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res[0]->count;
		}

	}

?>