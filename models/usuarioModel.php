<?php

	class loginModel{

		private $id;
		private $nombre;
		private $clave;
		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function setId($id){
			$this->id = $id;
		}
		public function setNombre($nombre){
			$this->nombre = $nombre;
		}
		public function setClave($clave){
			$this->clave = $clave;
		}

		public function index(){
			include_once('./views/loginView.php');
		}

		public function check(){
			try{
				$res = $this->db->prepare('SELECT * FROM usuario WHERE nombre = ?');
				$res->execute([$this->nombre]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return $res;
			}else{
				return 'no';
			}
		}
		
		

	}

?>