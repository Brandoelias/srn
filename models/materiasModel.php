<?php

	class materiasModel{

		private $id;
		private $nombre;
		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function setId($id){
			$this->id = $id;
		}
		public function setNombre($nombre){
			$this->nombre = $nombre;
		}

		public function getTodos(){
			try{
				$res = $this->db->prepare('SELECT * FROM materia');
				$res->execute();
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}

			return $res;
		}

		public function agregar(){
			try{
				$res = $this->db->prepare('INSERT INTO materia (nombre) VALUES(?)');
				$res->execute([$this->nombre]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function editar(){
			try{
				$res = $this->db->prepare('UPDATE materia SET nombre = ? WHERE id = ?');
				$res->execute([$this->nombre, $this->id]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function borrar(){
			try{
				$res = $this->db->prepare('DELETE FROM materia WHERE id = ?');
				$res->execute([$this->id]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function getGradoPorId($id){
			try{
				$res = $this->db->prepare('SELECT * FROM grado_materia,grado WHERE id_materia = ? AND grado_materia.id_grado = grado.id');
				$res->execute([$id]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function getGradosNotIn(){
			try{
				$res = $this->db->prepare('SELECT * FROM grado WHERE id NOT IN (SELECT id_grado FROM grado_materia WHERE id_materia = ?)');
				$res->execute([$this->id]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function agregarGrado($idGrado){
			try{
				$res = $this->db->prepare('INSERT INTO grado_materia(id_grado, id_materia) VALUES(?, ?)');
				$res->execute([$idGrado, $this->id]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function getGradosPorIdMateria(){
			try{
				$res = $this->db->prepare('SELECT * FROM grado_materia,grado WHERE id_materia = ? AND grado_materia.id_grado = grado.id');
				$res->execute([$this->id]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}

			return $res;
		}

		public function borrarGradoDeMateria($idGrado){
			try{
				$res = $this->db->prepare('DELETE FROM grado_materia WHERE id_grado = ? AND id_materia = ?');
				$res->execute([$idGrado, $this->id]);
				$this->db->query("DELETE FROM nota_materia WHERE id_grado = $idGrado AND id_materia = $this->id");
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function getMateriasPorGrado($id_grado){
			try{
				$res = $this->db->prepare('SELECT * FROM grado_materia,materia WHERE id_grado = ? AND grado_materia.id_materia = materia.id');
				$res->execute([$id_grado]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}

			return $res;
		}

	}


?>