<?php

	class calificacionesModel{

		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function guardarNotas($notas){
			foreach ($notas as $nota) {
				try{
					$res = $this->db->prepare('UPDATE nota_materia SET lapso1 = ?, lapso2 = ?, lapso3 = ?, recu1 = ?, recu2 = ? WHERE id_materia = ? AND ci_estudiante = ? AND id_grado = ?');
					$res->execute([$nota->lapso1, $nota->lapso2, $nota->lapso3, $nota->recu1, $nota->recu2, $nota->id_materia, $nota->cedula, $nota->id_grado]);
				}catch(Exception $e){
					error_log($e);
					return 'no';
				}
			}
			return 'ok';
		}

		public function getNotasPorCedula($cedula){
			try{
				$res = $this->db->prepare('SELECT * FROM nota_materia,materia WHERE nota_materia.id_materia = materia.id AND ci_estudiante = ?');
				$res->execute([$cedula]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 'no';
			}
			return $res;
		}

		public function getNotasPorCedulaYGrado($cedula, $id_grado){
			try{
				$res = $this->db->prepare('SELECT * FROM nota_materia,materia WHERE nota_materia.id_materia = materia.id AND ci_estudiante = ? AND id_grado = ?');
				$res->execute([$cedula, $id_grado]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 'no';
			}
			return $res;
		}

		public function getPeriodosDeCedulaYGrado($cedula, $id_grado){
			try{
				$res = $this->db->prepare('SELECT DISTINCT periodo FROM nota_materia WHERE ci_estudiante = ? AND id_grado = ?');
				$res->execute([$cedula, $id_grado]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 'no';
			}
			return $res;
		}

		public function getNotasPorCedulaYGradoYPeriodo($cedula, $id_grado, $periodo){
			try{
				$res = $this->db->prepare('SELECT * FROM nota_materia,materia WHERE nota_materia.id_materia = materia.id AND ci_estudiante = ? AND id_grado = ? AND periodo = ?');
				$res->execute([$cedula, $id_grado, $periodo]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 'no';
			}
			return $res;
		}

		public function getPeriodos(){
			try{
				$res = $this->db->prepare('SELECT DISTINCT periodo FROM nota_materia');
				$res->execute();
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 'no';
			}
			return $res;
		}

		public function getGradosPorPeriodo($periodo){
			try{
				$res = $this->db->prepare('SELECT DISTINCT grado.nombre,grado.id FROM nota_materia,grado WHERE grado.id = nota_materia.id_grado AND nota_materia.periodo = ?');
				$res->execute([$periodo]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 'no';
			}
			return $res;
		}

		public function getCedulasPorGradoYPeriodo($periodo, $id_grado){
			try{
				$res = $this->db->prepare('SELECT DISTINCT ci_estudiante,grado.nombre as nombreGrado FROM nota_materia,grado WHERE grado.id = nota_materia.id_grado AND periodo = ? AND id_grado = ?');
				$res->execute([$periodo, $id_grado]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 'no';
			}
			return $res;
		}

		public function consultarEstudiantePorCedula($ci_estudiante){
			try{
				$res = $this->db->prepare('SELECT * FROM estudiante,grado WHERE estudiante.id_grado = grado.id AND cedula = ?');
				$res->execute([$ci_estudiante]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 'no';
			}
			return $res[0];
		}

	}

?>