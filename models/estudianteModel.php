<?php


	class estudianteModel{

		private $cedula;
		private $nombres;
		private $apellidos;
		private $fecha_nacimiento;
		private $id_grado;
		private $seccion_numero;
		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function setCedula($cedula){
			$this->cedula = $cedula;
		}
		public function setNombres($nombres){
			$this->nombres = $nombres;
		}
		public function setApellidos($apellidos){
			$this->apellidos = $apellidos;
		}
		public function setFechaNacimiento($fecha){
			$this->fecha_nacimiento = $fecha;
		}
		public function setIdGrado($id){
			$this->id_grado = $id;
		}
		public function setSeccionNumero($n){
			$this->seccion_numero = $n;
		}

		public function getDatosPorCedula(){
			try{
				$res = $this->db->prepare('SELECT  * FROM estudiante WHERE cedula = ?');
				$res->execute([$this->cedula]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res[0];
		}

		public function getTodos(){
			try{
				$res = $this->db->prepare('SELECT DISTINCT * FROM estudiante,grado WHERE id_grado = grado.id');
				$res->execute();
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function guardar(){
			try{
				$res = $this->db->prepare('INSERT INTO estudiante (cedula, nombres, apellidos, fecha_nacimiento, id_grado, seccion_numero) VALUES(?, ?, ?, ?, ?, ?)');
				$res->execute([$this->cedula, $this->nombres, $this->apellidos, $this->fecha_nacimiento, 0, 0]);
			}catch(Exception $e){
				return 'no';
			}
			
			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function editar($cedula){
			try{
				$res = $this->db->prepare('UPDATE estudiante SET cedula = ?, nombres = ?, apellidos = ?, fecha_nacimiento = ? WHERE cedula = ?');
				$res->execute([$cedula, $this->nombres, $this->apellidos, $this->fecha_nacimiento, $this->cedula]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function borrar(){
			try{
				$res = $this->db->prepare('DELETE FROM estudiante WHERE cedula = ?');
				$res->execute([$this->cedula]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function verGradoDeEstudiante(){
			try{
				$res = $this->db->prepare('SELECT grado.nombre FROM estudiante,grado WHERE estudiante.id_grado = grado.id OR estudiante.id_grado IS NULL AND estudiante.cedula = ?');
				$res->execute([$this->cedula]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function inscribirEstudianteAGrado(){
			try{
				$res = $this->db->prepare('UPDATE estudiante SET id_grado = ?, seccion_numero = ? WHERE cedula = ?');
				$res->execute([$this->id_grado, $this->seccion_numero, $this->cedula]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function borrarGradoDeEstudiante(){
			try{
				$res = $this->db->prepare('UPDATE estudiante SET id_grado = ?, seccion_numero = ? WHERE cedula = ?');
				$res->execute([0, 0, $this->cedula]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				$this->db->query("DELETE FROM materia_estudiante WHERE ci_estudiante = $this->cedula");
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function agregarMaterias($materias, $materiasArrastre, $periodo){
			foreach ($materias as $materia) {
				try{
					$res = $this->db->prepare('INSERT INTO materia_estudiante(id_materia, ci_estudiante, id_grado) VALUES(?, ?, ?)');
					$res->execute([$materia, $this->cedula, $this->id_grado]);
					try{
						$this->db->query("INSERT INTO nota_materia(id_materia, ci_estudiante, id_grado, periodo) VALUES($materia, $this->cedula, $this->id_grado, $periodo)");
					}catch(Exception $e){

					}
				}catch(Exception $e){
					error_log($e);
					return 'no';
				}
			}

			foreach ($materiasArrastre as $materia) {
				$id_grado = $this->id_grado - 1;
				try{
					$res = $this->db->prepare('INSERT INTO materia_estudiante(id_materia, ci_estudiante, id_grado) VALUES(?, ?, ?)');
					$res->execute([$materia, $this->cedula, $id_grado]);
					try{
						$this->db->query("INSERT INTO nota_materia(id_materia, ci_estudiante, id_grado, periodo) VALUES($materia, $this->cedula, $id_grado, $periodo)");
					}catch(Exception $e){

					}
				}catch(Exception $e){
					error_log($e);
					return 'no';
				}
			}
			return 'ok';
		}

		public function getCantidadDeMaterias(){
			try{
				$res = $this->db->prepare('SELECT COUNT(id_materia) FROM materia_estudiante WHERE ci_estudiante = ? AND id_grado IN(SELECT id_grado FROM estudiante WHERE cedula = ?)');
				$res->execute([$this->cedula, $this->cedula]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res[0]->count;
		}

		public function getCantidadDeMateriasDeArrastre(){
			try{
				$res = $this->db->prepare('SELECT COUNT(id_materia) FROM materia_estudiante WHERE ci_estudiante = ? AND id_grado NOT IN(SELECT id_grado FROM estudiante WHERE cedula = ?)');
				$res->execute([$this->cedula, $this->cedula]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res[0]->count;
		}

		public function getMaterias(){
			try{
				$res = $this->db->prepare('SELECT * FROM materia_estudiante,materia WHERE ci_estudiante = ? AND materia_estudiante.id_materia = materia.id AND id_grado IN(SELECT id_grado FROM estudiante WHERE cedula = ?)');
				$res->execute([$this->cedula, $this->cedula]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function getMateriasDeArrastre(){
			try{
				$res = $this->db->prepare('SELECT * FROM materia_estudiante,materia WHERE ci_estudiante = ? AND materia_estudiante.id_materia = materia.id AND id_grado NOT IN(SELECT id_grado FROM estudiante WHERE cedula = ?)');
				$res->execute([$this->cedula, $this->cedula]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function borrarMateriaDeEstudiante($id_materia, $id_grado){
			try{
				$res = $this->db->prepare('DELETE FROM materia_estudiante WHERE id_materia = ? AND ci_estudiante = ? AND id_grado = ?');
				$res->execute([$id_materia, $this->cedula, $id_grado]);
			}catch(Exception $e){
				return 'no';
			}

			if($res->rowCount() > 0){
				return 'ok';
			}else{
				return 'no';
			}
		}

		public function getMateriasNotIn($id_grado){
			try{
				$res = $this->db->prepare('SELECT * FROM materia WHERE materia.id NOT IN(SELECT id_materia FROM materia_estudiante WHERE ci_estudiante = ?) AND materia.id IN(SELECT id_materia FROM grado_materia WHERE id_grado = ?)');
				$res->execute([$this->cedula, $id_grado]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function getMateriasDeArrastreNotIn($id_grado){
			try{
				$res = $this->db->prepare('SELECT * FROM materia WHERE id IN(SELECT id_materia FROM grado_materia WHERE id_grado = ?) AND id NOT IN(SELECT id_materia FROM materia_estudiante WHERE ci_estudiante = ? AND id_grado = ?)');
				$res->execute([$id_grado, $this->cedula, $id_grado]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function getEstudiantesPorGradoYSeccion(){
			try{
				$res = $this->db->prepare('SELECT * FROM estudiante WHERE id_grado = ? AND seccion_numero = ?');
				$res->execute([$this->id_grado, $this->seccion_numero]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function getNotasMaterias(){
			try{
				$res = $this->db->prepare('SELECT * FROM nota_materia,materia WHERE ci_estudiante = ? AND nota_materia.id_materia = materia.id AND id_grado IN(SELECT id_grado FROM estudiante WHERE cedula = ?)');
				$res->execute([$this->cedula, $this->cedula]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function getNotasMateriasDeArrastre(){
			try{
				$res = $this->db->prepare('SELECT * FROM nota_materia,materia WHERE ci_estudiante = ? AND nota_materia.id_materia = materia.id AND id_grado NOT IN(SELECT id_grado FROM estudiante WHERE cedula = ?)');
				$res->execute([$this->cedula, $this->cedula]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'no';
			}
			return $res;
		}

		public function buscarAjax($dato){
			if(is_numeric($dato)){
				if(strlen((string)$dato) >= 5){
					$sql = 'SELECT * FROM estudiante,grado WHERE grado.id = estudiante.id_grado AND CAST(cedula AS TEXT) LIKE ?';
				}
				else{
					return [];
				}
			}else{
				$sql = 'SELECT * FROM estudiante,grado WHERE grado.id = estudiante.id_grado AND nombres LIKE ?';
			}
			try{
				$res = $this->db->prepare($sql);
				$res->execute([$dato.'%']);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return [];
			}
			return $res;
		}


	}


?>